function y = runfermi(beta)
nstep = 10^5;
t = 0 ; 
tvect = 1:nstep;
for i=1:nstep
    u = fermi_process(beta);
    tvect(i) = u(1);
end;
tvect;
y = mean(tvect)