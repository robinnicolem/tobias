function ret = fermi_fonc(pia,pib,beta,pm)
ret = 1 / (1+ exp(pm*beta*(pia-pib)));
