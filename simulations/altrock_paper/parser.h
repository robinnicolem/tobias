#include <iostream>
#include <sstream>
#include <cstdlib> 

class parser
{
  int argc_m;
  char **argv_m;
  int arg_parsed;
public:
  parser(int argc, char **argv)
  {
    argc_m = argc;
    argv_m = argv;
    arg_parsed = 0;
  };
  int arg_num()
  {
    std::cout << "number of parsed arguments : " << arg_parsed << std::endl;
    std::cout << "number of cl arguments : " << argc_m << std::endl;
    return argc_m;
  }

  bool all_parsed()
  {
    if (argc_m - 1 == arg_parsed) return true ;
    else return false;
  }

  double getDoub(const std::string str,double &input)
  {
    for(int i = 1; i <argc_m ; i++)
      {
	if ((argv_m[i] == str) && argc_m > i) 
	  {
	  input = atof(argv_m[i+1]);
	  arg_parsed = arg_parsed + 2 ;
	  std::cout << ">> reading command line argument " << str << " with value " << input << std::endl ;
	  return 0;
	  }
      }
    return 1;
  };
  int getInt(const std::string str, int &input)
  {
    for(int i = 1; i <argc_m ; i++)
      {
	if ((argv_m[i] == str) && argc_m > i)
	  { 
	    input = atoi(argv_m[i+1]);
	    arg_parsed = arg_parsed + 2 ;
	    std::cout << " >> reading command line argument " << str << " with value " << input << std::endl ;
	    return 0;
	  }
      }
    return 1;
  };

  bool isArg(const std::string str)
  {
      for(int i = 1; i <argc_m ; i++)
      {
	if ((argv_m[i] == str)) 
	  {
	    return true;
	    arg_parsed = arg_parsed + 1;
	  }
      }
      return false;
  }
  int getString(const std::string str,std::string &input)
  {
    for(int i = 1; i <argc_m ; i++)
      {
	if ((argv_m[i] == str) && argc_m > i)
	  { 
	    input = argv_m[i+1];
	    arg_parsed = arg_parsed + 2 ;
	    std::cout << ">> reading command line argument " << str << " with value " << input << std::endl ;
	    return 0;
	  }
      }
    return 1;
  };
};


