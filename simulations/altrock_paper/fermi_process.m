
function y = fermi_process(beta)
% Fermi process 
N = 100;
%i = 10 ;
a = 5;
b = 2 ;
c = 3;
i = N-1;
d = 1;
PiA = (i-1)/(N-1) *a + (N-i)/(N-1)*b ;
PiB = i/(N-1) *c + (N-i-1)/(N-1)*d ;
t=0;
prob = fermi_fonc(PiA,PiB,beta,+1);
% Simulation
%i = 5; % state of the system
while (and(i ~= N,i~=0) )
    PiA = (i-1)/(N-1) *a + (N-i)/(N-1)*b ;
    PiB = i/(N-1) *c + (N-i-1)/(N-1)*d ;
    prob = fermi_fonc(PiA,PiB,beta,+1);
    cumulp  = Ti(i,N,prob);
    cumulm  = Ti(i,N,1-prob);
    nojump = 1 - (cumulp + cumulm );
    % compute the number of time without jumps
    t = t + geornd(nojump)+1;
    r = rand(1);
    if r < cumulp/(1-nojump)
        i=i+1;
    else
        i=i-1;
    end
end
y = [t,i];