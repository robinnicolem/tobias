 #include <cmath>
#include <iostream>
#include <random> 
#include <fstream>
#include "parser.h"

using namespace std ;

// defining the random number generator
default_random_engine ran(time(0));
uniform_real_distribution<double> unif(0.0,1.0);

int geom_dist(const double p)
{
  return (int) floor(log(unif(ran))/log(p));
}

int main(void)
{
  const int nvalues = 100000;
  const double p = 0.8;
  for(int i = 0; i < nvalues ; i++) cout << geom_dist(p) << endl ;
  return 0;
}
