
 /*
 *  Modified version of the article of Altrock et al.
 *  to model the continuous time dynamic
 */

#include <cmath>
#include <iostream>
#include <random> 
#include <fstream>
#include "parser.h"

using namespace std ;

// defining the random number generator
default_random_engine ran(time(0));
uniform_real_distribution<double> unif(0.0,1.0);

double gillespie_time(const double a0)
{
  return 1./a0 * log(1./unif(ran));
}

int Deltai(const double Tp,const double Tm)
{
  const double norm = Tp + Tm ;
  const double rnum = unif(ran);
  if (rnum  < Tp/norm)
    {
      return 1;
    }
  else 
    {
      return -1;
    }
}

double fermi_fonc(const double arg,const double beta)
{
  return 1 / (double)(1+ exp(-beta*arg));
}

double Ti(const int i, const int N, const double pi)
{
  return (double)i/((double)N)*(double)(N-i)/ ((double) N)* pi;
}

vector<double> fermi_simulation(const double beta, const double a, const double b, const double c, const double d,  const int N, const int initstate,const bool PrintValues = false)
{ 
  if (PrintValues)
    {
      cout << "a = " << a << endl ;
      cout << "b = " << b << endl ;
      cout << "c = " << c << endl ;
      cout << "d = " << d << endl ;
    }
  int i = initstate;
  double PiA = (double)(i-1)/(double)(N-1) *a + (double)(N-i)/(double)(N-1)*b ;
  double PiB = (double)i/(double)(N-1) *c + (double)(N-i-1)/(double)(N-1)*d ;
  double t=0 ;
  double prob = fermi_fonc(PiA-PiB,beta);
  double jump = 0 ; // sum of the transition rate
  double Tm,  Tp ; // proba to jump to +(p) or -
  double deltaT = 0 ; 
  double dI = 0 ; 
  while ((i !=  N )&& (i!=0))
    {
      PiA = (double)(i-1)/(double)(N-1) *a + (double)(N-i)/(double)(N-1)*b ;
      PiB = (double)i/(double)(N-1) *c + (double)(N-i-1)/(double)(N-1)*d ;
      prob = fermi_fonc(PiA - PiB,beta);
      //cout << prob << endl ;
      Tp  = Ti(i,N,prob);
      Tm  = Ti(i,N,1-prob); 
      jump = Tp + Tm ;
      deltaT = gillespie_time(jump);
      //cout << njump << endl ;
      t = t + deltaT;
      dI = Deltai(Tp,Tm);
      i = i +  dI ;
    } 
  vector<double> output(2);
  output[0] = t ;
  if (i == N) output[1] = 1;
  else output[1] = 0;
  return output;
}
int help()
{
  cout << "Computing the unconditionnal fixation times of a fermi process" << endl ;
  cout << "##############################################################" << endl ;
  cout << "\t --help display this help page" << endl ;
  cout << "\t --beta value of the beta in the simulation (default 0.05)" << endl ;
  cout << "\t --nsteps number of simulation steps (default 1000)" << endl ;
  cout << "\t --fname output file where the fixation times will be saved" << endl ;
  cout << "\t --a --b --c --d value of the payoff matrix {{a,b},{c,d}} default {{5,2},{3,1}}" << endl;
  cout << "\t --init initial number of players of type A (default 1)" << endl;
  cout << "\t --N total number of players (default 100)" << endl;
  return 0;
}

int param_display(const double beta, const int nstep, const string fname)
{
  cout << "value of beta " << beta << endl;
  cout << "number of steps" << nstep << endl ;
  cout << "output file :" << fname << endl ;
  return 0;
}
int main(int argc, char* argv[])
{
  parser options(argc,argv);
  if (options.isArg("--help")) 
    {
      help();
      return 0;
    }
  double a = 5,b = 2,c = 3 ,d = 1;
  double beta = 0.005;
  int N = 100;
  int init = 1 ;
  int nstep = 1e7; 
  string fname("times.dat");
  // BEGIN parsing parameters
  options.getDoub("--beta",beta);
  options.getDoub("--a",a);
  options.getDoub("--b",b);
  options.getDoub("--c",c);
  options.getDoub("--d",d);
  options.getInt("--nsteps",nstep);
  options.getString("--fname",fname);
  options.getInt("--N",N);
  options.getInt("--init",init);
  ofstream o_file(fname);
  if (!options.all_parsed())
    {
      cerr << "ERROR: One of the command line argument is does not correspond to an option" << endl << endl ;
      help() ;
      return 0;
    }
  vector<double> sim_output(2);
  param_display(beta,nstep,fname);
  // END parsing parameters
  sim_output = fermi_simulation(beta,a,b,c,d,N,init,true);
  o_file << sim_output[0] << "\t" << sim_output[1] << endl ;
  for (int i =0; i < nstep; i++)
    {
      // percentage counter
      if ( i % (int) 10000 == 0  )
	{ 	
	  cout <<"Simulation percentage "<< (double)i/(double)nstep * 100  << " %" << "\r" << flush;
	  
	}
      // END percentage counter
      sim_output = fermi_simulation(beta,a,b,c,d,N,init,false);
      o_file << sim_output[0] << "\t" << sim_output[1] << endl;
    }
}

