#include<iostream>
#include<cmath>
#include<random> 
#include<cmath>
#include<fstream>
#include<sstream>
#include "parser.h"
#include "EvoProcess.cpp"
#include "AdaptationDyn.cpp"
#include "ScDyn.cpp"
#include "debug_func.cpp"
using namespace std;
string NumToString(int& inp)
{
  ostringstream convert;
  convert << inp;      // insert the textual representation of 'Number' in the ch
  return convert.str(); // set 'Result' to the contents of the stream
}

struct GiResults
{
  int nsteps;
  double time;
  int x;
  int y;
};

struct TrajOutput
{
  bool output ;
  string fname ;
};


// declaring functions
inline bool stopcond(const int i, const int j,const int N);
inline double get_a0(const vector<Trate>& trans);
inline int get_reac_number(const double& r2,const vector<Trate>& trate,const double& a0);
inline int cout_Trate(const vector<Trate> x);
inline GiResults Gillespie(EvoProcess& proc,int i, int j, int N,mt19937& rng);
int cout_Gi(GiResults arg);


//BEGIN CODE
inline bool stopcond(const int i, const int j,const int N)
{
  // condition: the boundaries have been reached
  if ((i == 0) && (j == 0)) return false;
  else if ((i == 0) && (j == N)) return false;
  else if ((i == N) && (j == 0)) return false;
  else if ((i == N) && (j == N)) return false;
  else return true ;
}
//compute a0
inline double get_a0(const vector<Trate>& trans)
{
  const int size = trans.size();
  double cumul = 0;
  for (int i = 0; i < size; ++i)
    {
      cumul += (trans[i]).T;
    }
  return cumul;
}

inline int get_reac_number(const double& r2,const vector<Trate>& trate,const double& a0)
{
  double cumul =(double) (trate[0].T)/(double)a0 ;
  int i = 0;
  while (r2 > cumul)
    {
      i ++;
      cumul += trate[i].T/a0;
    }
  return i;
}
inline int cout_Trate(const vector<Trate> x)
{
  int s = x.size();
  cout << "{";
  for(int i = 0; i < s; i++)
    {
      cout << x[i].T <<"," ;
    }
  cout << "}" << endl;
  return 0;
}

inline GiResults Gillespie(EvoProcess& proc,int i, int j, int N,mt19937& rng, TrajOutput& trajoutput)
{
  long double t = 0;
  int nstep = 0;
  // Gillespie simulation
  vector<Trate> trans;
  double a0, r1, r2, tau, ireac;
  uniform_real_distribution<double> unif(0.0,1.0);
  GiResults res; 
  ofstream traj;
  if (trajoutput.output) traj.open(trajoutput.fname);
  while(stopcond(i,j,N))
    {
      trans = proc.Trans(i,j,N);
      //cout_Trate(trans);
      a0 = get_a0(trans);
      r1 = unif(rng);
      r2 = unif(rng);
      tau = 1./a0 * log(1./r1);//compute the time of the next reaction
      if (tau != tau) 
	{
	  cout << "Problem with the transition rates" << endl ;
	  cout << "BEGIN display transition rates" << endl ;
	  cout_Trate(trans);
	  cout << "end display transition rates" << endl ;
	  cout << "{" <<a0 << "," << r1 << "}" << endl ; 
	  cout << "{" << i << "," << j << "}" << endl ;
	  tau = 0; 
	}
      ireac = get_reac_number(r2,trans,a0);
      //cout << ireac << endl;
      t = t + tau;
      i += trans[ireac].dx[0];
      j += trans[ireac].dx[1];
      nstep++;
      if (trajoutput.output) 
	{
	  traj << nstep << "\t" 
	       << t << "\t" 
	       << i << "\t"
	       << j << "\t" << endl ;       
	}
    }
  res.x = i ;
  res.y = j ;
  res.nsteps = nstep;
  res.time  = t ;
  return res ;
}
int cout_Gi(GiResults arg)
{
  cout << arg.nsteps << "\t" <<arg.time << "\t" << "{" << arg.x << "," << arg.y << "}" << endl ;
  return 0;
}
int help()
{
  cout << " Compute the fixation times in a 2x2m assymetric game within a fermi process"<< endl <<
       "================================================================================"<< endl ;
  cout << "--help " << " display this help message" << endl;
  cout << "--i0 " << "initial state of population 1 (int default 1)" << endl;
  cout << "--j0 " <<"initial state of population 2 (int default 1)" << endl;
  cout << "--Np " << "Number of players (int default 10)" << endl;
  cout << "--beta " << " selection parameter (double default 0.2)" << endl ;
  cout << "--lambda" << "lambda parameter in the S.C. dynamic (double default 0.)" << endl ;  
  cout << "--nsim " << " number of simulations (double default 100000)" << endl ;
  cout << " --a1 " ;
  cout << " --b1 " ;
  cout << " --c1 " ;
  cout << " --d1 " << " Matrix of payoff of the population 1 {{a1,b1},{c1,d1}}" << endl 
       << "\t \t (double default {1,-1},{-1,1}}) " << endl ;
  cout << " --a2 " ;
  cout << " --b2 " ;
  cout << " --c2 " ;
  cout << " --d2 " << " Matrix of payoff of the population 2 {{a2,b2},{c2,d2}} "  << endl 
       << "\t \t(double default {-1,1},{1,-1})"<< endl ;;
  cout << "--o" << " output file containinf the fixation times and corresponding state (string default \"ftime.dat \")" << endl ;
  cout << "--ntraj output the first n drajectories: default value 0" << endl ; 
  return 0;
}
// there is an issue with the point (50,50)
int main(int argc, char *argv[])
{
  // starting random number generator
  unsigned seed(time(0));
  mt19937 rng(seed);
  uniform_real_distribution<double> unif(0.0,1.0);
  int i = 1, j = 1;
  int N = 10;
  int nsim = 10000;
  double beta = 0.2;
  double a1 = 1, b1 = -1, c1 = -1, d1 = 1;
  double a2 = -1,b2 = 1, c2 = 1, d2 = -1;
  double lambda = 0.;
  string ofname = "ftime.dat";
  int ntraj = 0;
  /**********************
   * BEGIN parsing input
   **********************/
  parser input(argc,argv); //creating the parser instance
  input.getDoub("--lambda",lambda);
  input.getInt("--i0",i);
  input.getInt("--j0",j);
  input.getInt("--Np",N);
  input.getDoub("--beta",beta);
  input.getDoub("--a1",a1);
  input.getDoub("--b1",b1);
  input.getDoub("--c1",c1);
  input.getDoub("--d1",d1);
  input.getDoub("--a2",a2);
  input.getDoub("--b2",b2);
  input.getDoub("--c2",c2);
  input.getDoub("--d2",d2);
  input.getString("--o",ofname);
  input.getInt("--nsim",nsim);
  input.getInt("--ntraj",ntraj);
  EvoProcess *process;
  cout << "{" << a1<< "," << b1<< "," << c1<< "," << d1<< "}" << endl
       << "{" << a2 << "," << b2<< "," << c2<< "," << d2 << "}" << endl ;
  if (input.isArg("--sc"))
    {
      cout << "Setting the process to be of the type SC" << endl ;
      process = new SCDyn(beta,{a1,b1,c1,d1},{a2,b2,c2,d2},lambda);
    }
  else
    {
      cout << "Setting the process to be an adaptation dynamic" << endl ;
      process = new AdaptationDyn(beta,{a1,b1,c1,d1},{a2,b2,c2,d2});
    }
  // checking if the arguments were parsed correctly#
  if (!(input.all_parsed()) || input.isArg("--help")) 
    {
      help();
      return 1;
    }
  /**********************
   * END parsing input
   **********************/
 
  ofstream ofile(ofname);
  // Gillespie simulation
  
  
  TrajOutput outputtraj;
  
  GiResults gires;
  ofile << "nsteps"  << "\t" << "time" << "\t" <<"x"<< "\t" << "y" << endl;
  for (int itt = 0; itt < nsim; ++itt)
    {
      if(itt < ntraj) 
	{
	  outputtraj.output = true ; 
	  outputtraj.fname = "traj"+NumToString(itt)+".dat";
	} 
      else 
	{
	  outputtraj.output = false ; 
	}
      cout << "\r Itterationg number "<< itt << "\t \t" << flush;  
      gires = Gillespie(*process,i,j,N,rng,outputtraj);
      ofile << gires.nsteps  << "\t" << gires.time << "\t" << gires.x<< "\t" << gires.y << endl;
      /* cout << "itteration number " << itt
	   << gires.nsteps  << "\t" << gires.time << "\t" << gires.x<< "\t" << gires.y << endl;
      */
    }
  return 0;
}

