#include <iostream>
#include <sstream>
#include <cstdlib> 

class parser
{
  int argc_m; //number of command line argument
  char **argv_m; //pointer to the command line argument
  int arg_parsed; // number of parsed arguments
public:
  /****************************
   * Example of the initialization of the parser class
   * int main(int argc, char* argv[])
   * {
   * parser options(argc,argv);
   * bla bla bla 
   * return 0;
   * }
   ****************************/
  parser(int argc, char **argv)
  {
    argc_m = argc;
    argv_m = argv;
    arg_parsed = 0;
  };
  // return the number of argument
  int arg_num()
  {
    std::cout << "number of parsed arguments : " << arg_parsed << std::endl;
    std::cout << "number of cl arguments : " << argc_m << std::endl;
    return argc_m;
  }
  // return true if all the command line arguments have been parsed
  bool all_parsed()
  {
    if (argc_m - 1 == arg_parsed) return true ;
    else return false;
  }
  /* read a double command line argumend e.g.
   * if command line was --double 23.2
   * parser.getDoub("--double",foo)
   * will put 23.2 in foo
   */
  double getDoub(const std::string str,double &input)
  {
    for(int i = 1; i <argc_m ; i++)
      {
	if ((argv_m[i] == str) && argc_m > i +1 ) 
	  {
	  input = atof(argv_m[i+1]);
	  arg_parsed = arg_parsed + 2 ;
	  std::cout << ">> reading command line argument " << str << " with value " << input << std::endl ;
	  return 0;
	  }
      }
    return 1;
  };
  /* read an integer command line argumend 
   * e.g.:
   * if command line was --Int 23 
   * parser.getInt("--Int",foo)
   * will put 23 in foo
   */
  int getInt(const std::string str, int &input)
  {
    for(int i = 1; i <argc_m ; i++)
      {
	if ((argv_m[i] == str) && argc_m > i + 1 )
	  { 
	    input = atoi(argv_m[i+1]);
	    arg_parsed = arg_parsed + 2 ;
	    std::cout << " >> reading command line argument " << str << " with value " << input << std::endl ;
	    return 0;
	  }
      }
    return 1;
  };
  /* return true if string was part of the command line arguments
   * e.g.:
   * parser.isArg("--hello")
   * will return true if --hello appeared in the command line
   */
  bool isArg(const std::string str)
  {
      for(int i = 1; i <argc_m ; i++)
      {
	if ((argv_m[i] == str)) 
	  {
	    arg_parsed++;
	    return true;
	  }
      }
      return false;
  }
  /* read a string command line argumend 
   * e.g.:
   * if command line was --Strin bonjour 
   * parser.getInt("--String",foo)
   * will put "bonjour" in foo
   */
  int getString(const std::string str,std::string &input)
  {
    for(int i = 1; i <argc_m ; i++)
      {
	if ((argv_m[i] == str) && argc_m > i + 1  )
	  { 
	    input = argv_m[i+1];
	    arg_parsed = arg_parsed + 2 ;
	    std::cout << ">> reading command line argument " << str << " with value " << input << std::endl ;
	    return 0;
	  }
      }
    return 1;
  };
};


