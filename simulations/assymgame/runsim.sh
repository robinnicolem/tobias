#!/bin/sh
./gillespie.out --Np 10 --i0 5 --j0 5 --o 10.dat4 --sc --lambda 0.4 --nsim 200 
./gillespie.out --Np 20 --i0 10 --j0 10 --o 20.dat4 --sc --lambda 0.4 --nsim 200
./gillespie.out --Np 30 --i0 15 --j0 15 --o 30.dat4 --sc --lambda 0.4 --nsim 200
./gillespie.out --Np 40 --i0 20 --j0 20 --o 40.dat4 --sc --lambda 0.4 --nsim 200
./gillespie.out --Np 50 --i0 25 --j0 25 --o 50.dat4 --sc --lambda 0.4 --nsim 200
./gillespie.out --Np 60 --i0 30 --j0 30 --o 60.dat4 --sc --lambda 0.4 --nsim 200
./gillespie.out --Np 70 --i0 35 --j0 35 --o 70.dat4 --sc --lambda 0.4 --nsim 200
./gillespie.out --Np 80 --i0 40 --j0 40 --o 80.dat4 --sc --lambda 0.4 --nsim 200
./gillespie.out --Np 120 --i0 60 --j0 60 --o 120.dat4 --sc --lambda 0.4 --nsim 200
./gillespie.out --Np 150 --i0 75 --j0 75 --o 150.dat4 --sc --lambda 0.4 --nsim 200
#./gillespie.out --Np 17 --i0 85 --j0 85 --o 170.dat4 --sc --lambda 0.4 --nsim 200


./gillespie.out --Np 10 --i0 5 --j0 5 --o 10.dat --sc --lambda 0. --nsim 200 
./gillespie.out --Np 20 --i0 10 --j0 10 --o 20.dat --sc --lambda 0. --nsim 200
./gillespie.out --Np 30 --i0 15 --j0 15 --o 30.dat --sc --lambda 0. --nsim 200
./gillespie.out --Np 40 --i0 20 --j0 20 --o 40.dat --sc --lambda 0. --nsim 200
./gillespie.out --Np 50 --i0 25 --j0 25 --o 50.dat --sc --lambda 0. --nsim 200
./gillespie.out --Np 60 --i0 30 --j0 30 --o 60.dat --sc --lambda 0. --nsim 200
./gillespie.out --Np 70 --i0 35 --j0 35 --o 70.dat --sc --lambda 0. --nsim 200
./gillespie.out --Np 80 --i0 40 --j0 40 --o 80.dat --sc --lambda 0. --nsim 200
./gillespie.out --Np 120 --i0 60 --j0 60 --o 120.dat --sc --lambda 0. --nsim 200
./gillespie.out --Np 150 --i0 75 --j0 75 --o 150.dat --sc --lambda 0. --nsim 200
#./gillespie.out --Np 17 --i0 85 --j0 85 --o 170.dat --sc --lambda 0. --nsim 200
