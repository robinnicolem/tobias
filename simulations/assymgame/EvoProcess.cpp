#ifndef EVOPROCESS_CPP
#define EVOPROCESS_CPP
using namespace std;
// Virtual class used as a base class to define an evolutionary process
struct Trate
{
  double T; // value of the transition rate
  vector<int> dx; //move associated to the transition
};
class EvoProcess
{
public:
  
  virtual vector<Trate> Trans(const int i,const int j, const int N) = 0 ;
};
#endif
