#ifndef ADDYN_CPP
#define ADDYN_CPP
class AdaptationDyn: public EvoProcess
{
public:
  AdaptationDyn(const double gamma,const vector<double> pay1, const vector<double> pay2);
  //Vector of transition rate to be returned to the Gillespie simulation
  vector<Trate> Trans(const int i,const int j, const int N) override;
  /**********************************************************************
   * Function: Poff1 
   * Arg {# of players from pop2 in state A, tot # of players in pop2}
   * output : {average payoff of a player from pop1 playing A, average payoff of a player from pop1 playing B}
   **********************************************************************/
  vector<double> Poff1(const int i,const int N);
  /**********************************************************************
   * Function Poff2
   * Arg {# of players from pop1 in state A, tot # of players in pop1}
   * output : {average payoff of a player from pop2 playing A, average payoff of a player from pop2 playing B}
   **********************************************************************/
  vector<double> Poff2(const int i,const int N);
  /********************************
   * Function SwitchProb
   * Arg: {average payoff in initial state (in), average payoff in the final state (fin)}
   * output: probability to switch from state (in) to (fin)  
   ********************************/
  double SwitchProb(const double x);
  /**********************************
   * Function T1
   * arg {#players from pop1 in state A(i), #players from pop2 in state A(j), total # of player in the two populations }
   * output {T_{i-> i+1}, T_{i-> i-1}}
   **********************************/
  vector<double> T1(const int i, const int j, const int N);
  /**********************************
   * Function T2
   * arg {#players from pop1 in state A(i), #players from pop2 in state A(j), total # of player in the two populations }
   * output {T_{j-> j+1}, T_{j-> j-1}}
   **********************************/
  vector<double> T2(const int i, const int j, const int N);
  /**********************************
   * HTheta 
   * description: heaviside theta function
   **********************************/
  double HTheta(const double x);
private:
  double Gamma ; // parameter of the Switching probability
  vector<double> Payoff1, Payoff2; //contains the matrix of the payoff of the normal form game played by 
};
AdaptationDyn::AdaptationDyn(const double gamma,const vector<double> pay1, const vector<double> pay2)
{
  Payoff1 = pay1;
  Payoff2 = pay2;
  Gamma = gamma;
}


vector<Trate> AdaptationDyn::Trans(const int i,const int j, const int N)
{
  vector<Trate> trans(4);
  vector<double> T1v = T1(i,j,N), T2v = T2(i,j,N);
  trans[0].T = T1v[0];
  trans[0].dx = {1,0};
  trans[1].T = T1v[1];
  trans[1].dx = {-1,0};
  trans[2].T = T2v[0];
  trans[2].dx = {0,1};
  trans[3].T = T2v[1];
  trans[3].dx = {0,-1};
  return trans;
}

inline vector<double> AdaptationDyn::T1(const int i, const int j, const int N)
{
  vector<double> P(Poff1(j,N));
  const double f = (double) i * (double)(N-i)/(double)N ;
  vector<double> output(2);
  output[0] = f * SwitchProb(P[0]-P[1]);
  output[1] = f * SwitchProb(P[1]-P[0]);
  return output;
}

inline vector<double> AdaptationDyn::T2(const int i, const int j, const int N)
{
  vector<double> P(Poff2(i,N));
  const double f = (double) j * (double)(N-j)/(double)N ;
  vector<double> output(2);
  output[0] = f * SwitchProb(P[0]-P[1]);
  output[1] = f * SwitchProb(P[1]-P[0]);
  return output;
}

inline vector<double> AdaptationDyn::Poff1(const int j,const int N)
 {
   vector<double> output(2);
   output[0] = (double) j/(double) N* Payoff1[0] + (double) (N-j)/(double) N *Payoff1[1];
   output[1] = (double) j/(double) N* Payoff1[1] + (double) (N-j)/(double) N *Payoff1[3];
   return output;
 }
inline vector<double> AdaptationDyn::Poff2(const int i,const int N)
 {
   vector<double> output(2);
   output[0] = (double) i/(double) N * Payoff2[0] + (double) (N-i)/(double) N * Payoff2[1];
   output[1] = (double) i/(double) N * Payoff2[1] + (double) (N-i)/(double) N * Payoff2[3];
   return output;
 }

inline double AdaptationDyn::SwitchProb(const double x)
{
  // Probability to go from A -> B in the adaptation dynamic 
  //return Gamma /(double)2 *  (PB -  PA) * HTheta(PB-PA);
  
  // case of a Fermi Process
  return 1. /(double)(1 +   exp(- Gamma * x));
}
inline double AdaptationDyn::HTheta(const double x)
{
  if (x >= 0) return x;
  else return 0;
  //if (x >= 0) return 1;
  //else return 1;
}

#endif  
