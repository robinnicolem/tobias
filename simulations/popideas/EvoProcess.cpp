#ifndef EVOPROCESS_CPP
#define EVOPROCESS_CPP
using namespace std;
// Virtual class used as a base class to define an evolutionary process
class EvoProcess
{
public:
  // virtual double Tp(const int i, const int N) = 0 ;
  virtual double Tm(const int i, const int N) = 0 ; // Transition rate from i to i+ 1 when there is N individuals in the system
  virtual double Tp(const int i, const int N) = 0 ; // transition rate from i  to i+1 when there is N individuals
  ~EvoProcess()
  {
    cout << "Base destructor called" << endl ;
  } ;
};
#endif
