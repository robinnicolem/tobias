/*
 *  Modified version of the article of Altrock et al.
 *  to model the continuous time dynamic
 */


#include <cmath>
#include <iostream>
#include <random> 
#include <fstream>
#include "parser.h"
#include "FermiProcess.cpp"
#include "ScProcess.cpp"
#include "MutProcess.cpp"
#include "ScProcessFermi.cpp"
#include <string>
// defining the random number generator
default_random_engine ran(time(0));
uniform_real_distribution<double> unif(0.0,1.0);

struct PrintTraj
{
  bool isprinted ; 
  string filename;
};

double gillespie_time(const double a0)
{
  /******************************
   * computing the time of 
   * the next process which 
   * occurs with rate a0 in the 
   * Gillespie simulation 
   * =======================
   * input: rate a0 
   * output: time before the process will occur
   ******************************/
  return 1./(double)a0 * log((double)1./(double)unif(ran));
}

int Deltai(const double Tp,const double Tm)
{
  /*******************************
   * Compute the next process 
   * which will occur in the
   * Gillespie simulation
   * ==========================
   * imput: 
   * - Tm : rate of i -> i-1 
   * - Tp : rate of i ->  i+1 
   * output: 
   * -  (- 1) if i-> i-1 is the next process to happen 
   * - +1 if i -> i+1  is the next process to happen
   *******************************/
  if (Tp < 0 || Tm < 0 ) 
    {
      cout << "ERROR" << endl;
      return 0;
    }
  const double norm = Tp + Tm ;
  const double rnum = unif(ran);
  if (rnum  < (double)Tp/(double)norm)
    {
      return 1;
    }
  else 
    {
      return -1;
    }
}

vector<double> Gillespie(EvoProcess &process, const int N, const int initstate,const bool PrintValues ,const PrintTraj pt)
{ 
  /**********************************************
   * Gillespie algorithm to compute
   * the first passage tie in the state 0 or N
   * =============================================
   * output: {first passage time, staten 0 for 0 and 1 for N}
   *==============================================
   * parameters:
   * - Evo Process &process: class which defines the one step process to be used
   * - int N : number of players 
   * - initial number of players using strategy A
   * - PrintValues : set to True if you want to print debug infos
   **********************************************/
  
  int i = initstate; // initial sate 
  double t=0 ; // time variabel 
  double totrate = 0 ; // sum of the transition rate
  double Tm,  Tp ; // proba to jump to +(p) or -
  double deltaT = 0 ; // time between two events
  double dI = 0 ;// state difference \in {\pm 1}
  int nstep  = 0 ;
  ofstream traj ;
  if(pt.isprinted) traj.open(pt.filename);
  while ((i !=  N )&& (i!=0) && (i != -1))
    {
      nstep ++;
      //computing the transition rates;
      Tp  = process.Tp(i,N); 
      Tm  = process.Tm(i,N); 
      totrate = Tp + Tm ; //compute the jump rate
      deltaT = gillespie_time(totrate); // time before the next event 
      t = t + deltaT; // update time 
      dI = Deltai(Tp,Tm); // is the event a jump i -> i+1 or i-> i-1 
      i = i +  dI ;// updateing system state
      /* If one of the transition rates is negative 
       * the fixation time is NAN and the fixation state is -1*/
      if (dI == 0 ) 
	{
	  t = NAN;
	  i = -1;
	}
      if(pt.isprinted) traj << nstep << "\t" 
			    << t << "\t" 
			    << i << endl ;
    } 
  // generating output vector
  vector<double> output(3);
  output[0] = t ;
  if (i == N) output[1] = 1;
  else if (i == 0) output[1] = 0;
  else output[0] = -1;
  output[2] = nstep ;
  traj.close();
  return output; 
}
int help()
{
  /*********************************************
   * Help message of the code, this is called 
   * the programm is called with --h or with a
   *wrong argument
   *********************************************/
  cout << "Computing the unconditionnal fixation times of a fermi process" << endl ;
  cout << "##############################################################" << endl ;
  cout << "\t --help display this help page" << endl ;
  cout << "\t --beta value of the beta in the simulation (default 0.05) (FERMI)" << endl ;
  cout << "\t --nsteps number of simulation steps (default 1000)" << endl ;
  cout << "\t --fname output file where the fixation times will be saved" << endl ;
  cout << "\t --a --b --c --d value of the payoff matrix {{a,b},{c,d}} default {{5,2},{3,1}}" << endl;
  cout << "\t --init initial number of players of type A (default 1)" << endl;
  cout << "\t --N total number of players (default 100)" << endl;
  cout << "\t --procname name of the evolutionary to be used choice : fermi(default), sc, or mut" << endl ;
  cout << "\t --lambda parameter (SC) (default 0)" << endl ;
  cout << "\t --u parameter (MUT) (default 0)" << endl ;
  cout << "\t --GameName the name of the game to be played in {cx,coord}" << endl ;
  cout << "\t ======================="<< endl;
  cout << "\t transfer rate of the evolutionary processes: " << endl;
  cout << "\t sc: T_{i -> j} \\propto 0.5 (1 + \\gamma (f_i - f_j )) " << endl ;
  cout << "\t scfermi T_{i -> j} \\propto fermi_function(delta_pi)" << endl;
  cout << "\t  with f_i = \\pi_i - \\lambda log(x_i) " << endl ;
  cout << "\t fermi: T_{i -> j} \\propto  1 /(1+exp(\\beta(f_i - f_j ))) " << endl ;
  cout << "\t \t with f_i = \\pi_i" << endl ;
  cout << "\t mutator equation:  T_{i -> j} \\propto 0.5 (1 + \\gamma (f_i - f_j )) " << endl ;
  cout << "\t A mutation also occurs at rate u" << endl ;
  cout << "\t --ntraj number of trajectories to be saved" << endl ;
  return 0;
}

int param_display(const double beta, const int nstep, const string fname)
{
  
  cout << "value of beta " << beta << endl;
  cout << "number of steps " << nstep << endl ;
  cout << "output file :" << fname << endl ;
  return 0;
}
int main(int argc, char* argv[])
{
  parser options(argc,argv);
  if (options.isArg("--help")) 
    {
      help();
      return 0;
    }


  // default parameters
  double a = 5,b = 2,c = 3 ,d = 1;
  double  lambda = 0.;
  double beta = 1.;
  int N = 100;
  int init = 1 ;
  int nstep = 1e5; 
  string fname("times.dat");
  string procname("fermi");
  double u = 0;
  string gname = "dom";
  int ntraj = 0;
  
  // BEGIN parsing command line parameters
  options.getDoub("--beta",beta);
  options.getDoub("--a",a);
  options.getDoub("--b",b);
  options.getDoub("--c",c);
  options.getDoub("--d",d);
  options.getInt("--nsteps",nstep);
  options.getString("--fname",fname);
  options.getInt("--N",N);
  options.getInt("--init",init);
  options.getDoub("--lambda",lambda);
  options.getString("--procname",procname);
  options.getDoub("--u",u);
  options.getInt("--ntraj",ntraj);
  ofstream o_file(fname);
  options.getString("--GameName",gname);
  if (gname == "coord")
    {
      a = 5;
      b = 1;
      c = 3;
      d  = 2;
    }
  if (gname == "cx")
    {
      a = 3;
      b = 3;
      c = 5;
      d = 1;
    }
  // display an erro message if one of the command line parameter is not valid
  if (!options.all_parsed())
    {
      cerr << "ERROR: One of the command line argument does not correspond to an option" << endl << endl ;
      help() ;
      return 0;
    }
  
  // Creating a class corresponding to the process to be simulated
  EvoProcess *process; // Creating pointer to the parent class 
  vector<double> sim_output(2);
  
  param_display(beta,nstep,fname); //  displaying the parameters
  
  // parsing the name of the simulation to be used 
  // and creating the class oif the corresponding process
  if (procname == "fermi")
    {
      cout << "Using a fermi process" << endl ;
      process = new FermiProcess(beta,a,b,c,d);
    }
  else if (procname == "sc")
    {
      cout << "Simulating a Sato Crutchfield type dynamic" << endl ;
      process = new ScProcess(beta,lambda,a,b,c,d);
    }
  else if (procname == "mut")
    {
      cout << "Simulating a mutator type dynamic" << endl ;
      process = new MutProcess(beta,u,a,b,c,d);
    }
  else if (procname == "scfermi")
    {
      cout << "Simulating a fermi type sc process" << endl ;
      process = new ScProcessFermi(beta,lambda,a,b,c,d);
    }
  else 
    {
      cout << "Please enter a valid name of process: \"sc\", \"fermi\" or \"mut\"" << endl;
      help();
      return 0;
    }
  //END Parsing parameters
  PrintTraj pt = {.isprinted = true, .filename = "traj0.dat"};
  // BEGIN GILLESPIE SIMULATION
  sim_output = Gillespie(*process,N,init,true,pt); // intialize the simulation step 
  o_file << sim_output[0] << "\t" << sim_output[1] << "\t" << sim_output[1] << endl ;
  for (int i =0; i < nstep; i++)
    {
      if (i < ntraj)
	{
	  pt = {.isprinted = true, .filename = "traj"+to_string(i)+".dat"};
	}
      else
	{
	  pt.isprinted = false;
	}
      if ( i % (int) 10000 == 0  )
	{ 	
	  // Diplaying the evolution of the simulation
	  cout <<"Simulation percentage "<< (double)i/(double)nstep * 100  << " %" << "\r" << flush;
	}
      sim_output = Gillespie(*process,N,init,false,pt); // running one simulation
      //break the loop if the transition rates are negative
      if (sim_output[0] != sim_output[0])
	{
	  cerr << "ERROR: Negative transition rates" << endl ;
	  o_file << "ERROR: Negative transition rates" << endl ;
	  o_file << sim_output[0] << "\t" << sim_output[1] << "\t" << sim_output[2] << endl; // put results in a file
	  break;
	}
      o_file << sim_output[0] << "\t" << sim_output[1] << "\t" << sim_output[2] << endl; // put results in a file
    }
  // END GILLESPIE SIMULATION
  return 0;   
}

