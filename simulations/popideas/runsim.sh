
#for gamma in 0.0 0.001 0.002 0.003 0.004 0.005 0.006 0.007 0.008 0.009 0.01 
#do 
#    echo "value of gamma " $gamma
#    echo ./simu.out --procname sc --fname outputdom_gamma_$gamma.dat --gamma $gamma --a 5 --b 2 --c 3 --d 1  
#    ./simu.out --procname sc --fname outputdom_gamma_$gamma.dat --gamma $gamma --a 5 --b 2 --c 3 --d 1
#done 
for gamma in 0.0 0.001 0.002 0.003 0.004 0.005 0.006 0.007 0.008 0.009 0.01 
do 
    ./simu.out --procname sc --fname outputcoexls_gamma_$gamma.dat --gamma $gamma --a 3 --b 2 --c 5 --d 1 --lambda 0
done  
#for gamma in 0.0 0.001 0.002 0.003 0.004 0.005 0.006 0.007 0.008 0.009 0.01 
#do 
#    ./simu.out --procname sc --fname outputcoord_gamma_$gamma.dat --gamma $gamma --a 5 --b 1 --c 3 --d 2
#done 
