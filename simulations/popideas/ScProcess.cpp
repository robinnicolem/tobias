#ifndef ScProcess_CPP
#define ScProcess_CPP
// Define an evolutionary process,
// child class of EvoProcess
class ScProcess: public EvoProcess
{
public:
  double lambda,gamma,a ,b,c,d;
  ScProcess(const double gamma_a,const double lambda_a,const double a_a,const double b_a,const double c_a,const double d_a)
  {
    gamma = gamma_a ;
    lambda = lambda_a;
    a = a_a;
    b = b_a;
    c = c_a;
    d = d_a;
  }
  ~ScProcess()
  {
    delete &lambda ;
    delete &lambda ;
    delete &a;
    delete &b;
    delete &c;
    delete &d;
  }
  void init(const double gamma_a,const double lambda_a,const double a_a,const double b_a,const double c_a,const double d_a)
  {
    gamma = gamma_a ;
    lambda = lambda_a;
    a = a_a;
    b = b_a;
    c = c_a;
    d = d_a;
  }
  inline double Ti(const int i, const int N, const double pi)
  {
    return (double)i*(double)(N-i)/((double)N)*0.5 *( 1 + 0.5* gamma * pi);
  };
  inline double PayoffA(const double i,const double N)
  {
    // Modified fitness which is payoff - lambda * log((double)(i)/(double) N) ;
    return  (double)(i-1)/(double)(N-1) *a + (double)(N-i)/(double)(N-1)*b  - lambda * log((double)i/(double) N);
  };
  inline double PayoffB(const double i,const double N)
  {
    // Modified fitness which is payoff - lambda * log((double)(N-i)/(double) N) ;
    return (double)i/(double)(N-1) *c + (double)(N-i-1)/(double)(N-1)*d - lambda * log((double)(N-i)/(double) N) ;
  };

  double Tp(const int i, const int N) override
  {
    const double PiA = PayoffA(i,N);
    const double PiB = PayoffB(i,N);
    const double prob =PiA - PiB ;
    return Ti(i,N,prob);
  };

  double Tm(const int i, const int N) override
  {
    const double PiA = PayoffA(i,N);
    const double PiB = PayoffB(i,N) ;
    const double prob =-PiA + PiB ;
    return Ti(i,N,prob);
  };
};

#endif
