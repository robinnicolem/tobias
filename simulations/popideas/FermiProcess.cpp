#ifndef FERMI_CPP
#define FERMI_CPP
#include "EvoProcess.cpp"
using namespace std;
class FermiProcess: public EvoProcess
{
public:
  double beta ;
  double a,b,c,d;
  FermiProcess(const double beta_a,const double a_a,const double b_a,const double c_a,const double d_a)
  {
    beta = beta_a ;
    a = a_a;
    b = b_a;
    c = c_a;
    d = d_a;
  }
  ~FermiProcess() 
  {
    delete &beta;
    delete &a;
    delete &b;
    delete &c;
    delete &d;
  };
  void init(const double beta_a,const double a_a,const double b_a,const double c_a,const double d_a)
  {
    beta = beta_a ;
    a = a_a;
    b = b_a;
    c = c_a;
    d = d_a;
  }
  double fermi_fonc(const double arg)
  {
    return 1. / (double)(1+ exp(-beta*arg));
  };
  
  double Ti(const int i, const int N, const double pi) 
  {
    // transition rate which scale like N
    return (double)i* (double)(N-i)/ ((double) N)* pi;
  };
  
  double Tp(const int i, const int N) override
  {
    const double PiA = (double)(i-1)/(double)(N-1) *a + (double)(N-i)/(double)(N-1)*b ;
    const double PiB = (double)i/(double)(N-1) *c + (double)(N-i-1)/(double)(N-1)*d ; 
    const double prob = fermi_fonc(PiA - PiB);
    return Ti(i,N,prob);
  };
  double Tm(const int i, const int N) override 
  {
    const double PiA = (double)(i-1)/(double)(N-1) *a + (double)(N-i)/(double)(N-1)*b ;
    const double PiB = (double)i/(double)(N-1) *c + (double)(N-i-1)/(double)(N-1)*d ; 
    const double prob = fermi_fonc(PiA - PiB);
    return Ti(i,N,1-prob);
  };
};
#endif
