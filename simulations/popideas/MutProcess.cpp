#ifndef MutProcess_CPP
#define MutProcess_CPP

class MutProcess: public EvoProcess
{
public:
  double u, gamma, a, b, c ,d;
  MutProcess(const double gamma_a,const double u_a,const double a_a,const double b_a,const double c_a,const double d_a)
  {
    gamma = gamma_a ;
    u = u_a;
    a = a_a;
    b = b_a;
    c = c_a;
    d = d_a;
  }
  ~MutProcess() 
  {
    delete &u ;
    delete &a;
    delete &b;
    delete &c;
    delete &d;
  }
  void init(const double gamma_a,const double u_a,const double a_a,const double b_a,const double c_a,const double d_a)
  {
    gamma = gamma_a ;
    u = u_a;
    a = a_a;
    b = b_a;
    c = c_a;
    d = d_a;
  }
  inline double Ti(const int i, const int N, const double pi) 
  {
    return (double)i * (double)(N-i)/ ((double) N)* 0.5*( 1 + gamma * pi);
  };
  inline double PayoffA(const double i,const double N)
  {
    return  (double)(i-1)/(double)(N-1) *a + (double)(N-i)/(double)(N-1)*b ;
  };
  inline double PayoffB(const double i,const double N)
  {
    return (double)i/(double)(N-1) *c + (double)(N-i-1)/(double)(N-1)*d  ;
  };
  double mutation(const int i, const int N)
  {
    return i * (i-1)/((double)N*(double) N) * u/(double)2+ 
      (N-i) * (N- i - 1)/((double)N*(double) N) *u/(double)2;
  }
  double Tp(const int i, const int N) override
  {
    const double PiA = PayoffA(i,N);
    const double PiB = PayoffB(i,N); 
    const double prob =PiA - PiB ;
    return Ti(i,N,prob)*(1 - u/(double)2)+ // Probability to switch without mituations
      mutation(i,N); 
      // switch when matching two agents of the same population + mutation
    
  };
  
  double Tm(const int i, const int N) override
  {
    const double PiA = PayoffA(i,N);
    const double PiB = PayoffB(i,N) ;
    const double prob =-PiA + PiB ;
    return Ti(i,N,prob) *(1 - u/(double)2)+ // Probability to switch without mituations
      mutation(i,N); ;
  };
};
#endif
