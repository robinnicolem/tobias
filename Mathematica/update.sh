#!/bin/sh
ODIR=~/Dropbox/Nicole_Sollich_Galla/robin/Mathematica/
# copy the files
cp fixation_time.nb  $ODIR
cp flow_diagram.nb $ODIR
cp flow_diagram.pdf $ODIR
cp AssymGames.nb $ODIR
