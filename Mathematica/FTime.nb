(* Content-type: application/vnd.wolfram.mathematica *)

(*** Wolfram Notebook File ***)
(* http://www.wolfram.com/nb *)

(* CreatedBy='Mathematica 10.0' *)

(*CacheID: 234*)
(* Internal cache information:
NotebookFileLineBreakTest
NotebookFileLineBreakTest
NotebookDataPosition[       158,          7]
NotebookDataLength[     10503,        318]
NotebookOptionsPosition[      9519,        281]
NotebookOutlinePosition[      9855,        296]
CellTagsIndexPosition[      9812,        293]
WindowFrame->Normal*)

(* Beginning of Notebook Content *)
Notebook[{
Cell[BoxData[
 RowBox[{"<<", "FermiVsLin.nb"}]], "Input",
 CellChangeTimes->{{3.655462221419149*^9, 3.655462227726959*^9}}],

Cell[CellGroupData[{

Cell["Computing the fixation time", "Title",
 CellChangeTimes->{{3.655462091289113*^9, 3.655462098928877*^9}}],

Cell["\<\

computation of the fixation time\
\>", "Text",
 CellChangeTimes->{{3.655462101473518*^9, 3.655462117864991*^9}}],

Cell[CellGroupData[{

Cell[BoxData[{
 RowBox[{"NaVal", "=", 
  RowBox[{"{", 
   RowBox[{
   "100", ",", "200", ",", "300", ",", "500", ",", "800", ",", "1000", ",", 
    "1200"}], "}"}]}], "\[IndentingNewLine]", 
 RowBox[{"{", 
  RowBox[{
   RowBox[{"\[Beta]", "=", "0.1"}], ",", 
   RowBox[{"\[Lambda]", "=", "0.1"}]}], "}"}], "\[IndentingNewLine]", 
 RowBox[{"Precision", "[", "\[Beta]", "]"}], "\[IndentingNewLine]", 
 RowBox[{"NaVal2", "=", 
  RowBox[{"(", 
   RowBox[{
    RowBox[{
     RowBox[{"#", "+", "1"}], "&"}], "/@", "NaVal"}], ")"}]}]}], "Input",
 CellChangeTimes->{{3.655462111778432*^9, 3.6554621469621572`*^9}, {
  3.65546221703454*^9, 3.6554622172569323`*^9}, {3.655462251447473*^9, 
  3.6554622701535378`*^9}}],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{
  "100", ",", "200", ",", "300", ",", "500", ",", "800", ",", "1000", ",", 
   "1200"}], "}"}]], "Output",
 CellChangeTimes->{{3.655462114377639*^9, 3.655462147688818*^9}, {
   3.6554622548173647`*^9, 3.655462270903751*^9}, 3.655462722391307*^9}],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{"0.1`60.", ",", "0.1`60."}], "}"}]], "Output",
 CellChangeTimes->{{3.655462114377639*^9, 3.655462147688818*^9}, {
   3.6554622548173647`*^9, 3.655462270903751*^9}, 3.6554627223926687`*^9}],

Cell[BoxData["60.`"], "Output",
 CellChangeTimes->{{3.655462114377639*^9, 3.655462147688818*^9}, {
   3.6554622548173647`*^9, 3.655462270903751*^9}, 3.655462722393744*^9}],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{
  "101", ",", "201", ",", "301", ",", "501", ",", "801", ",", "1001", ",", 
   "1201"}], "}"}]], "Output",
 CellChangeTimes->{{3.655462114377639*^9, 3.655462147688818*^9}, {
   3.6554622548173647`*^9, 3.655462270903751*^9}, 3.655462722395054*^9}]
}, Open  ]],

Cell[BoxData[{
 RowBox[{"ftime", "=", 
  RowBox[{
   RowBox[{
    RowBox[{"{", 
     RowBox[{
      RowBox[{
       RowBox[{"Print", "[", "#", "]"}], ";", "#"}], ",", 
      RowBox[{"ObtainMaxFtime", "[", 
       RowBox[{"\[Beta]", ",", "\[Lambda]", ",", "#"}], "]"}]}], "}"}], "&"}],
    "/@", "NaVal"}]}], "\[IndentingNewLine]", 
 RowBox[{"ftime1", "=", 
  RowBox[{
   RowBox[{
    RowBox[{"{", 
     RowBox[{
      RowBox[{
       RowBox[{"Print", "[", "#", "]"}], ";", "#"}], ",", 
      RowBox[{"ObtainMaxFtime", "[", 
       RowBox[{"\[Beta]", ",", "\[Lambda]", ",", "#"}], "]"}]}], "}"}], "&"}],
    "/@", "NaVal2"}]}], "\[IndentingNewLine]"}], "Input",
 CellChangeTimes->{{3.65546229077391*^9, 3.655462301518977*^9}}],

Cell[CellGroupData[{

Cell[BoxData[{
 RowBox[{
  RowBox[{
   RowBox[{"{", 
    RowBox[{"x", ",", "y1", ",", "y2"}], "}"}], "=", 
   RowBox[{"{", 
    RowBox[{
     RowBox[{
      RowBox[{
       RowBox[{"#", "[", 
        RowBox[{"[", "1", "]"}], "]"}], "&"}], "/@", "ftime"}], ",", 
     RowBox[{
      RowBox[{
       RowBox[{"#", "[", 
        RowBox[{"[", "2", "]"}], "]"}], "&"}], "/@", "ftime"}], ",", 
     RowBox[{
      RowBox[{
       RowBox[{"#", "[", 
        RowBox[{"[", "2", "]"}], "]"}], "&"}], "/@", "ftime1"}]}], "}"}]}], 
  ";"}], "\[IndentingNewLine]", 
 RowBox[{"ListPlot", "[", 
  RowBox[{"Table", "[", 
   RowBox[{
    RowBox[{"{", 
     RowBox[{
      RowBox[{"x", "[", 
       RowBox[{"[", "i", "]"}], "]"}], ",", 
      RowBox[{
       RowBox[{"(", 
        RowBox[{"y2", "-", "y1"}], ")"}], "[", 
       RowBox[{"[", "i", "]"}], "]"}]}], "}"}], ",", 
    RowBox[{"{", 
     RowBox[{"i", ",", "1", ",", 
      RowBox[{"Length", "[", "x", "]"}]}], "}"}]}], "]"}], 
  "]"}], "\[IndentingNewLine]", 
 RowBox[{"ListPlot", "[", 
  RowBox[{"ftimepot", ",", 
   RowBox[{"PlotStyle", "\[Rule]", " ", "Red"}]}], "]"}]}], "Input",
 CellChangeTimes->{{3.6554624613664513`*^9, 3.6554626376068497`*^9}}],

Cell[BoxData[
 RowBox[{
  StyleBox[
   RowBox[{"N", "::", "precsm"}], "MessageName"], 
  RowBox[{
  ":", " "}], "\<\"Requested precision \[NoBreak]\\!\\(16\\)\[NoBreak] is \
smaller than $MinPrecision. Using $MinPrecision instead. \
\\!\\(\\*ButtonBox[\\\"\[RightSkeleton]\\\", ButtonStyle->\\\"Link\\\", \
ButtonFrame->None, ButtonData:>\\\"paclet:ref/message/General/precsm\\\", \
ButtonNote -> \\\"N::precsm\\\"]\\)\"\>"}]], "Message", "MSG",
 CellChangeTimes->{{3.65546260733463*^9, 3.65546263877413*^9}}],

Cell[BoxData[
 GraphicsBox[{{}, {{}, 
    {RGBColor[0.368417, 0.506779, 0.709798], PointSize[0.019444444444444445`],
      AbsoluteThickness[1.6], 
     PointBox[{{100., 0.01868142599027617}, {200., 0.018957487503231812`}, {
      300., 0.020529891086427}, {500., 0.02178474285738771}, {800., 
      0.022344890599505993`}, {1000., 0.02251305567040764}, {1200., 
      0.022621126410700826`}}]}, {}}, {}},
  AspectRatio->NCache[GoldenRatio^(-1), 0.6180339887498948],
  Axes->{True, True},
  AxesLabel->{None, None},
  AxesOrigin->{0, 0.01848444096925494},
  DisplayFunction->Identity,
  Frame->{{False, False}, {False, False}},
  FrameLabel->{{None, None}, {None, None}},
  FrameTicks->{{Automatic, Automatic}, {Automatic, Automatic}},
  GridLines->{None, None},
  GridLinesStyle->Directive[
    GrayLevel[0.5, 0.4]],
  Method->{},
  PlotRange->{{0, 1200.}, {0.01868142599027617, 0.022621126410700826`}},
  PlotRangeClipping->True,
  PlotRangePadding->{{
     Scaled[0.02], 
     Scaled[0.02]}, {
     Scaled[0.05], 
     Scaled[0.05]}},
  Ticks->{Automatic, Automatic}]], "Output",
 CellChangeTimes->{{3.6554625959081697`*^9, 3.65546263879307*^9}}],

Cell[BoxData[
 RowBox[{
  StyleBox[
   RowBox[{"N", "::", "precsm"}], "MessageName"], 
  RowBox[{
  ":", " "}], "\<\"Requested precision \[NoBreak]\\!\\(16\\)\[NoBreak] is \
smaller than $MinPrecision. Using $MinPrecision instead. \
\\!\\(\\*ButtonBox[\\\"\[RightSkeleton]\\\", ButtonStyle->\\\"Link\\\", \
ButtonFrame->None, ButtonData:>\\\"paclet:ref/message/General/precsm\\\", \
ButtonNote -> \\\"N::precsm\\\"]\\)\"\>"}]], "Message", "MSG",
 CellChangeTimes->{{3.65546260733463*^9, 3.6554626388850727`*^9}}],

Cell[BoxData[
 GraphicsBox[{{}, {{}, 
    {RGBColor[1, 0, 0], PointSize[0.019444444444444445`], AbsoluteThickness[
     1.6], PointBox[{{100., 0.01633988603040328}, {200., 
      0.01979107039516783}, {300., 0.021308184568150527`}, {500., 
      0.022745844460851924`}, {800., 0.02369175833014542}, {1000., 
      0.024040376699760137`}, {1200., 0.024285334290731087`}}]}, {}}, {}},
  AspectRatio->NCache[GoldenRatio^(-1), 0.6180339887498948],
  Axes->{True, True},
  AxesLabel->{None, None},
  AxesOrigin->{0, 0.01594261361738689},
  DisplayFunction->Identity,
  Frame->{{False, False}, {False, False}},
  FrameLabel->{{None, None}, {None, None}},
  FrameTicks->{{Automatic, Automatic}, {Automatic, Automatic}},
  GridLines->{None, None},
  GridLinesStyle->Directive[
    GrayLevel[0.5, 0.4]],
  Method->{},
  PlotRange->{{0, 1200.}, {0.01633988603040328, 0.024285334290731087`}},
  PlotRangeClipping->True,
  PlotRangePadding->{{
     Scaled[0.02], 
     Scaled[0.02]}, {
     Scaled[0.05], 
     Scaled[0.05]}},
  Ticks->{Automatic, Automatic}]], "Output",
 CellChangeTimes->{{3.6554625959081697`*^9, 3.6554626389178762`*^9}}]
}, Open  ]],

Cell[BoxData[
 RowBox[{"Clear", "[", "x", "]"}]], "Input",
 CellChangeTimes->{{3.655462735387539*^9, 3.655462736907854*^9}}],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"ftimepot", "=", 
  RowBox[{
   RowBox[{
    RowBox[{"{", 
     RowBox[{"#", ",", 
      RowBox[{"2", "*", 
       RowBox[{"PotTscaling", "[", 
        RowBox[{"\[Beta]", ",", "\[Lambda]", ",", "#"}], "]"}]}]}], "}"}], 
    "&"}], "/@", "NaVal"}]}]], "Input",
 CellChangeTimes->{{3.655462693821375*^9, 3.6554627084608307`*^9}, {
  3.655462744413103*^9, 3.655462747219542*^9}, {3.655466926985918*^9, 
  3.655466927137641*^9}}],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{
   RowBox[{"{", 
    RowBox[{"100", ",", "0.016339886030403282`"}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{"150", ",", "0.01848790069596396`"}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{"200", ",", "0.01979107039516783`"}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{"300", ",", "0.021308184568150527`"}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{"400", ",", "0.022176755140112706`"}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{"500", ",", "0.022745844460851913`"}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{"600", ",", "0.023150441657006027`"}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{"1000", ",", "0.024040376699760137`"}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{"1200", ",", "0.024285334290731087`"}], "}"}]}], "}"}]], "Output",\

 CellChangeTimes->{{3.655462429597115*^9, 3.655462440067288*^9}, {
  3.6554626973395348`*^9, 3.655462748768964*^9}, {3.655466924465167*^9, 
  3.655466929044602*^9}}]
}, Open  ]]
}, Open  ]]
},
WindowSize->{958, 1094},
WindowMargins->{{Automatic, 0}, {Automatic, 0}},
FrontEndVersion->"10.0 for Linux x86 (64-bit) (December 4, 2014)",
StyleDefinitions->"Default.nb"
]
(* End of Notebook Content *)

(* Internal cache information *)
(*CellTagsOutline
CellTagsIndex->{}
*)
(*CellTagsIndex
CellTagsIndex->{}
*)
(*NotebookFileOutline
Notebook[{
Cell[558, 20, 123, 2, 32, "Input"],
Cell[CellGroupData[{
Cell[706, 26, 110, 1, 96, "Title"],
Cell[819, 29, 123, 4, 50, "Text"],
Cell[CellGroupData[{
Cell[967, 37, 707, 18, 99, "Input"],
Cell[1677, 57, 286, 6, 32, "Output"],
Cell[1966, 65, 227, 4, 55, "Output"],
Cell[2196, 71, 171, 2, 65, "Output"],
Cell[2370, 75, 286, 6, 32, "Output"]
}, Open  ]],
Cell[2671, 84, 725, 21, 77, "Input"],
Cell[CellGroupData[{
Cell[3421, 109, 1194, 38, 77, "Input"],
Cell[4618, 149, 509, 10, 23, "Message"],
Cell[5130, 161, 1149, 28, 234, "Output"],
Cell[6282, 191, 512, 10, 23, "Message"],
Cell[6797, 203, 1129, 27, 233, "Output"]
}, Open  ]],
Cell[7941, 233, 124, 2, 32, "Input"],
Cell[CellGroupData[{
Cell[8090, 239, 448, 12, 32, "Input"],
Cell[8541, 253, 950, 24, 55, "Output"]
}, Open  ]]
}, Open  ]]
}
]
*)

(* End of internal cache information *)
