(* ::Package:: *)

BeginPackage["FtimeSym`"]
DomGame::usage = "Define a Dominance Game"
CoexGame::usage = "Defines a Cexistance Game"
CoordGame::usage = "Define a Coordination Game"
Tip::usage ="Tip[beta_,i_,Na_,a_,b_,c_,d_,SProbp_,SProbm_] returns the transition rate from i to i+1"
Tim::usage ="Tim[beta_,i_,Na_,a_,b_,c_,d_,SProbp_,SProbm_] returns the transition rate from i to i-1"
DeltaPi::usage = "DeltaPi[a,b,c,d,i,Na],Payoff difference to be used in the fdermi function"
Fprob::usage = "Fprob[beta,a,b,c,d,Na,SProbp,SProbm,Tip,Tim] fixation probability vector 
	Alternative deinition: Fprob[beta,Na,SProbp,SProbm,Tip,Tim] fixation probability vector"
UncondFixationTime::usage = "UncondFixationTime[beta,a,b,c,d,Na,SProbp,SProbm]compute the 
					uncconditional fixation time. Sprobm and Sprobp are the switching probability to i-1 or i+1"
CondFixationTime::usage = "\!\(\*
StyleBox[\"Computation\",\nFontWeight->\"Bold\"]\)\!\(\*
StyleBox[\" \",\nFontWeight->\"Bold\"]\)\!\(\*
StyleBox[\"of\",\nFontWeight->\"Bold\"]\)\!\(\*
StyleBox[\" \",\nFontWeight->\"Bold\"]\)\!\(\*
StyleBox[\"the\",\nFontWeight->\"Bold\"]\)\!\(\*
StyleBox[\" \",\nFontWeight->\"Bold\"]\)\!\(\*
StyleBox[\"fixation\",\nFontWeight->\"Bold\"]\)\!\(\*
StyleBox[\" \",\nFontWeight->\"Bold\"]\)\!\(\*
StyleBox[\"time\",\nFontWeight->\"Bold\"]\)\!\(\*
StyleBox[\":\",\nFontWeight->\"Bold\"]\)
	Alternative arguments : -CondFixationTime[beta_,Na_,SProbp_,SProbm_,Tip_,Tim;]
		                     -CondFixationTime[beta_,a_,b_,c_,d_,Na_,SProbp_,SProbm_,Tip_,Tim_]
Compute the conditional fixation time. Sprobm and Sprobp are the switching probability to i-1 or i+1"
LogM::usage = "Modified logarithm function to return 1 instead of Infinity in 0 and Infinity"
SCFoncP::usage = "SCFoncP[arg_,beta_,i_,Na_,lambda_] i -> i+1 Sato Cruchtfeld transition rate with \[Lambda] as a SC variable (entropic term)"
SCFoncM::usage = "SCFoncM[arg_,beta_,i_,Na_,lambda_] i -> i-1 Sato Cruchtfeld transition rate with \[Lambda] as a SC variable (entropic term)"
CustomGame::usage = "CustomGame[a,b,c,d] set the default game to be the symmetric game with payoff matrix {{a,b},{c,d}}";
FprobTrate::usage ="FprobTrate[beta_,a_,b_,c_,d_,Na_,Tpf_,Tmf_]"
CondFixationTimeTrate::usage ="CondFixationTimeTrate[beta_,a_,b_,c_,d_,Na_,Tpf_,Tmf_]"
UncondFixationTimeTrate::usage ="UncondFixationTimeTrate[beta_,a_,b_,c_,d_,Na_,Tpf_,Tmf_]"
TpMut::usage = "TpMut[beta_,i_,Na_,a_,b_,c_,d_]"
TmMut::usage ="TpMut[beta_,i_,Na_,a_,b_,c_,d_]"
ScEquation::usage = "ScEquation[\[Lambda]]: Sato cruchtfeld equation with variable x and entropic factor \[Lambda]"
MutEquation::usage = "MutEquation[u_]: mutator equation with variable x and mutation rate u"
GetFixedPoints::usage = "GetFixedPoints[exp_,lambda_] get the fixed point of the replicator equation and return:\!\(\*
StyleBox[\" \",\nFontWeight->\"Bold\"]\)\!\(\*
StyleBox[\"{\",\nFontWeight->\"Bold\"]\)\!\(\*
StyleBox[\"\[Lambda]\",\nFontWeight->\"Bold\"]\)\!\(\*
StyleBox[\",\",\nFontWeight->\"Bold\"]\)\!\(\*
StyleBox[\"fixed\",\nFontWeight->\"Bold\"]\)\!\(\*
StyleBox[\" \",\nFontWeight->\"Bold\"]\)\!\(\*
StyleBox[\"point\",\nFontWeight->\"Bold\"]\)\!\(\*
StyleBox[\",\",\nFontWeight->\"Bold\"]\)\!\(\*
StyleBox[\"stab\",\nFontWeight->\"Bold\"]\)\!\(\*
StyleBox[\"}\",\nFontWeight->\"Bold\"]\)
						where \!\(\*
StyleBox[\"stab\",\nFontWeight->\"Bold\"]\) is equal to \!\(\*
StyleBox[\"True\",\nFontWeight->\"Bold\"]\)\!\(\*
StyleBox[\" \",\nFontWeight->\"Bold\"]\)if the point is stable and \!\(\*
StyleBox[\"False\",\nFontWeight->\"Bold\"]\)\!\(\*
StyleBox[\" \",\nFontWeight->\"Bold\"]\)othervise"
PlotFixedPointsSc::usage ="PlotFixedPointsSc[name_] or PlotFixedPointsSc[name,\[Lambda]min,\[Lambda]max,step] will do a plot of the fixed points of the deterministic Sc dynamic with different values opf \[Lambda]"
PlotFixedPointsMut::usage ="PlotFixedPointsMut[name_,\[Lambda]min_,\[Lambda]max_,step_],  PlotFixedPointsMut[name_], PlotFixedPointsMut[]"
HeatMapFpointsSc::usage = "HeatMapFpointsSc[\[Beta]_,Npl_,fonc_]"
FermiFonc::usage = "fermi functions"
FermiFoncP::usage = "fermi functions"
FermiFoncM::usage = "fermi functions"
FlowDiag1D::usage = "eg: FlowDiag1D[ScEquation[0.2,x]"
GetFPListSc::usage ="Same as GetFixedPointsSc but provide only a table of the dfixed points"
GetFPListMut::usage = "List of the fixed points of the mutator equation {unstable points, stable points}"
Begin["`Private`"]

(*define the transition rates*)
CustomGame[an_?NumericQ,bn_?NumericQ,cn_?NumericQ,dn_?NumericQ]:={a=an,b=bn,c=cn,d=dn};
DomGame:=(Clear[a,b,c,d,GameName];a=5;b=2;c=3;d=1;GameName = "Dominance game";)
CoexGame:=(Clear[a,b,c,d,GameName];a=3;b=2;c=5;d=1;GameName = "Coexsitence game";)
CoordGame:=(Clear[a,b,c,d,GameName];a=5;b=1;c=3;d=2;GameName = "Coordination game";)
(*General transition probability*)
Tip[beta_?NumericQ,i_?NumericQ,Na_?NumericQ,a_?NumericQ,b_?NumericQ,c_?NumericQ,d_?NumericQ,SProbp_,SProbm_]:=
i/Na*(Na-i)*(SProbp[DeltaPi[a,b,c,d,i,Na],beta,i,Na]);
Tim[beta_?NumericQ,i_?NumericQ,Na_?NumericQ,a_?NumericQ,b_?NumericQ,c_?NumericQ,d_?NumericQ,SProbp_,SProbm_]:=
i/Na*(Na-i)*(SProbm[DeltaPi[a,b,c,d,i,Na],beta,i,Na]);
DeltaPi=Function[{a,b,c,d,i,Na},
Module[{u,v},
u=(a+d-b-c)/Na;
v=(b-d);
u*i+v]
];
FermiFonc[arg_?NumericQ,beta_?NumericQ,i_?NumericQ,Na_?NumericQ]:=(1/(1+Exp[-beta*arg]));
FermiFoncP[arg_?NumericQ,beta_?NumericQ,i_?NumericQ,Na_?NumericQ]:=FermiFonc[arg,beta,i,Na];
FermiFoncM[arg_?NumericQ,beta_?NumericQ,i_?NumericQ,Na_?NumericQ]:=FermiFonc[arg,-beta,i,Na];
(*SC wswitching probability*)
LogM[x_?NumericQ]:=(
Module[{ret},If[x == 0 ,ret = 1;];
If[x == Infinity,ret = 1;];
If[x != 0&&x!=Infinity ,ret = Log[x];];
ret
]
);
SCFoncP[arg_?NumericQ,beta_?NumericQ,i_?NumericQ,Na_?NumericQ,lambda_?NumericQ]:=(0.5*(1+beta*(arg - lambda*LogM[(i)/(Na-i)])))//Quiet;
SCFoncM[arg_?NumericQ,beta_?NumericQ,i_?NumericQ,Na_?NumericQ,lambda_?NumericQ]:=0.5*(1-beta*(arg- lambda*LogM[(i)/(Na-i)]))//Quiet;
SCFoncP[\[Lambda]_]:=(SCFoncP[#1,#2,#3,#4,\[Lambda]]&);
SCFoncM[\[Lambda]_]:=(SCFoncM[#1,#2,#3,#4,\[Lambda]]&);
(*Mutator transition rates*)
TpMut[u_]:=(TpMut[#1,#2,#3,#4,#5,#6,#7,u]&);
TmMut[u_]:=(TmMut[#1,#2,#3,#4,#5,#6,#7,u]&);
TpMut[beta_,i_,Na_,a_,b_,c_,d_,u_]:=
i/Na*(Na-i)/Na*1/2/(1+beta*DeltaPi[a,b,c,d,i,Na])*(1-u/2)+u/(2*Na*Na)*(i*(i-1));
TmMut[beta_,i_,Na_,a_,b_,c_,d_,u_]:=
i/Na*(Na-i)/Na*1/2/(1-beta*DeltaPi[a,b,c,d,i,Na])*(1-u/2)+u/(2*Na*Na)*((Na-i)*(Na-i-1));
(*Computing the fixatino probability*)
Fprob[beta_,Na_,SProbp_,SProbm_,Tip_,Tim_]:= Fprob[beta,a,b,c,d,Na,SProbp,SProbm,Tip,Tim];
Fprob[beta_,a_,b_,c_,d_,Na_,SProbp_,SProbm_,Tip_,Tim_]:=
Module[{Tm,Tp,vars,eqs,repl},
FoncName="Fixation probability";
Tm  = Table[Tim[beta,i,Na,a,b,c,d,SProbp,SProbm],{i,1,Na-1}];
Tp = Table[Tip[beta,i,Na,a,b,c,d,SProbp,SProbm],{i,1,Na-1}];
vars = Table[Subscript[\[Phi],i],{i,1,Na+1}];
eqs = Table[-Tm[[i]]*(vars[[i+1]]-vars[[i]])+Tp[[i]]*(vars[[i+2]]-vars[[i+1]]),{i,1,Na-1}];
repl ={ vars[[1]]-> 0,vars[[Na+1]]-> 1};
eqs=(#==0)&/@(eqs/.repl);
vars/.Flatten[Solve[eqs,vars[[2;;Na]]]~Join~repl]
];
UncondFixationTime[beta_?NumericQ,Na_?NumericQ,SProbp_,SProbm_,Tip_,Tim_]:=UncondFixationTime[beta,a,b,c,d,Na,SProbp,SProbm,Tip,Tim];
UncondFixationTime[beta_?NumericQ,a_?NumericQ,b_?NumericQ,c_?NumericQ,d_?NumericQ,Na_?NumericQ,SProbp_,SProbm_,Tip_,Tim_]:=
Module[{Tm,Tp,vars,eqs,repl,sol},
FoncName="Unconditional fixation time";
Tm  = Table[Tim[beta,i,Na,a,b,c,d,SProbp,SProbm],{i,1,Na-1}];
Tp = Table[Tip[beta,i,Na,a,b,c,d,SProbp,SProbm],{i,1,Na-1}];
vars = Table[Subscript["uft",i-1],{i,1,Na+1}];
eqs = Table[vars[[j+1]]==1+vars[[j]]*Tm[[j]]+(1-Tm[[j]]-Tp[[j]])*vars[[j+1]]+Tp[[j]]*vars[[j+2]],{j,1,Na-1}];
repl ={ vars[[1]]-> 0,vars[[Na+1]]-> 0};
eqs=(eqs/.repl)//Simplify;
sol=Solve[eqs,vars[[2;;Na]]];
vars/.Flatten[sol~Join~repl]
];
CondFixationTime[beta_?NumericQ,Na_?NumericQ,SProbp_,SProbm_,Tip_,Tim_]:=CondFixationTime[beta,a,b,c,d,Na,SProbp,SProbm,Tip,Tim];
CondFixationTime[beta_?NumericQ,a_?NumericQ,b_?NumericQ,c_?NumericQ,d_?NumericQ,Na_?NumericQ,SProbp_,SProbm_,Tip_,Tim_]:=
Module[{Tm,Tp,vars,eqs,repl,sol,phi},
FoncName="Conditional fixation time";
Tm  = Table[Tim[beta,i,Na,a,b,c,d,SProbp,SProbm],{i,1,Na}];
Tp = Table[Tip[beta,i,Na,a,b,c,d,SProbp,SProbm],{i,1,Na}];
phi=Fprob[beta,a,b,c,d,Na,SProbp,SProbm,Tip,Tim];
vars = Table[Subscript["cft",i],{i,1,Na+1}];
eqs = Table[phi[[j+1]]*vars[[j+1]]==
(1+vars[[j]])*Tm[[j]]*phi[[j]]+
phi[[j+1]]*(1-Tm[[j]]-Tp[[j]])*(vars[[j+1]]+1)+
Tp[[j]]*phi[[j+2]]*(vars[[j+2]]+1),{j,1,Na-1}];
repl ={ vars[[1]]-> 0,vars[[Na+1]]-> 0};
eqs=(eqs/.repl)//Simplify;
sol=Solve[eqs,vars[[2;;Na]]];
vars/.Flatten[sol~Join~repl]
];
FprobTrate[beta_?NumericQ,Na_?NumericQ,Tpf_?NumericQ,Tmf_?NumericQ]:=FprobTrate[beta,a,b,c,d,Na,Tpf,Tmf]
FprobTrate[beta_?NumericQ,a_?NumericQ,b_?NumericQ,c_?NumericQ,d_?NumericQ,Na_,Tpf_,Tmf_]:=
Module[{Tp,Tm,vars,eqs,repl},
Tm  = Table[Tpf[beta,i,Na,a,b,c,d],{i,1,Na}];
Tp = Table[Tmf[beta,i,Na,a,b,c,d],{i,1,Na}];
vars = Table[Subscript[\[Phi],i],{i,1,Na}];
eqs = Table[-Tm[[i]]*(vars[[i]]-vars[[i-1]])+Tp[[i]]*(vars[[i+1]]-vars[[i]]),{i,2,Na-1}];
repl ={ vars[[1]]-> 0,vars[[Na]]-> 1};
eqs=(#==0)&/@(eqs/.repl);
vars/.Flatten[Solve[eqs,vars[[2;;Na-1]]]~Join~repl]
];
CondFixationTimeTrate[beta_,Na_,Tpf_,Tmf_]:=CondFixationTimeTrate[beta,a,b,c,d,Na,Tpf,Tmf]
CondFixationTimeTrate[beta_,a_?NumericQ,b_?NumericQ,c_?NumericQ,d_?NumericQ,Na_,Tpf_,Tmf_]:=
Module[{Tm,Tp,vars,eqs,repl,sol,phi},
Tm  = Table[Tpf[beta,i,Na,a,b,c,d],{i,1,Na}];
Tp = Table[Tmf[beta,i,Na,a,b,c,d],{i,1,Na}];
phi=FprobTrate[beta,a,b,c,d,Na,Tpf,Tmf];
vars = Table[Subscript["cft",i],{i,1,Na}];
eqs = Table[phi[[j]]*vars[[j]]==
(1+vars[[j-1]])*Tm[[j]]*phi[[j-1]]+
phi[[j]]*(1-Tm[[j]]-Tp[[j]])*(vars[[j]]+1)+
Tp[[j]]*phi[[j+1]]*(vars[[j+1]]+1),{j,2,Na-1}];
repl ={ vars[[1]]-> 0,vars[[Na]]-> 0};
eqs=(eqs/.repl)//Simplify;
sol=Solve[eqs,vars[[2;;Na-1]]];
vars/.Flatten[sol~Join~repl]
];
UncondFixationTimeTrate[beta_,Na_,Tpf_,Tmf_]:=UncondFixationTimeTrate[beta,a,b,c,d,Na,Tpf,Tmf]
UncondFixationTimeTrate[beta_,a_?NumericQ,b_?NumericQ,c_?NumericQ,d_?NumericQ,Na_,Tpf_,Tmf_]:=
Module[{Tm,Tp,vars,eqs,repl,sol},
Tm  = Table[Tpf[beta,i,Na,a,b,c,d],{i,1,Na}];
Tp = Table[Tmf[beta,i,Na,a,b,c,d],{i,1,Na}];
vars = Table[Subscript["uft",i],{i,1,Na}];
eqs = Table[vars[[j]]==1+vars[[j-1]]*Tm[[j]]+(1-Tm[[j]]-Tp[[j]])vars[[j]]+Tp[[j]]*vars[[j+1]],{j,2,Na-1}];
repl ={ vars[[1]]-> 0,vars[[Na]]-> 0};
eqs=(eqs/.repl)//Simplify;
sol=Solve[eqs,vars[[2;;Na-1]]];
vars/.Flatten[sol~Join~repl]
];

(*** Fixed point diagram of the deterministic replicaotor equation ****)
PiA[x_]:=x*a+(1-x)*b;
PiB[x_]:=x*c+(1-x)*d;


ScEquation[lambda_]:= x*(PiA[x]- (x*PiA[x]+(1-x)*PiB[x])) - lambda*x*(Log[x]-x*Log[x]-(1-x)*Log[1-x])
ScEquation[lambda_,x_]:= x*(PiA[x]- (x*PiA[x]+(1-x)*PiB[x])) - lambda*x*(Log[x]-x*Log[x]-(1-x)*Log[1-x])
MutEquation[u_]:=Module[{S = 2(*number of strategies*)},
 (1-u/2)*x*(PiA[x]- (x*PiA[x]+(1-x)*PiB[x])) +u/2*(1/S-x)
(*(1-u)*x*(PiA- (x*PiA+(1-x)*PiB)) +u*(x*PiA+(1-x)*PiB)*(1/S-x)*)(*g_{ik} = \pi_i / \pi*)
]
GetFixedPoints[exp_,lambda_]:=
(*Output:
{value of lambda, fixed point, True if it is stable, false else}
*)
Module[{Npoints=80,inc,fp,eps=10^-3,phi,totest},
phi[u_] = D[exp,x]/.{x-> u};
totest[u_]=Re[exp/.{x-> u}];
inc=Table[i/Npoints,{i,-1/2,Npoints-1}]//N;
fp = x/.FindRoot[exp,{x,#}]&/@inc;
fp = DeleteDuplicates[fp,Abs[#1-#2]<eps&];
fp = DeleteCases[fp,u_/;(Abs[Re[totest[u]]]>eps)];
{lambda,Re[#],Re[phi[#]]<0}&/@fp
];
PlotFixedPointsSc[]:= PlotFixedPointsSc[GameName,0,2,00.01];
PlotFixedPointsSc[name_]:= PlotFixedPointsSc[name,0,2,00.01];
PlotFixedPointsSc[name_,\[Lambda]min_,\[Lambda]max_,step_]:=
Module[{toplot,stableToplot,unstableToplot,plot},toplot=Flatten[Table[GetFixedPoints[ScEquation[lambda],lambda],{lambda,\[Lambda]min,\[Lambda]max,step}],1]//Quiet;
stableToplot={#[[1]],#[[2]]}&/@Select[toplot,(#[[3]]==True)&];
unstableToplot={#[[1]],#[[2]]}&/@Select[toplot,((#[[3]]==False))&];
plot =ListPlot[{Select[unstableToplot,#[[2]] >=  0&],Select[stableToplot,#[[2]]>=  0&]},PlotStyle-> {Blue,Red},AxesLabel-> {"\[Lambda]","fixed point"},PlotLegends-> {"unstable","stable"},PlotLabel->name];
Show[plot]
];
GetFPListSc[]:= GetFPListSc[GameName,0,2,00.01];
GetFPListSc[name_]:= GetFPListSc[name,0,2,00.01];
GetFPListSc[name_,\[Lambda]min_,\[Lambda]max_,step_]:=
Module[
{stableToplot,unstableToplot,toplot},toplot=Flatten[Table[GetFixedPoints[ScEquation[lambda],lambda],{lambda,\[Lambda]min,\[Lambda]max,step}],1]//Quiet;
stableToplot={#[[1]],#[[2]]}&/@Select[toplot,(#[[3]]==True)&];
unstableToplot={#[[1]],#[[2]]}&/@Select[toplot,((#[[3]]==False))&];
{Select[unstableToplot,#[[2]] >=  0&],Select[stableToplot,#[[2]]>=  0&]}
]
GetFPListMut[]:= GetFPListMut[GameName,0,2,00.01];
GetFPListMut[name_]:= GetFPListMut[name,0,2,00.01];
GetFPListMut[name_,\[Lambda]min_,\[Lambda]max_,step_]:=
Module[
{stableToplot,unstableToplot,toplot},toplot=Flatten[Table[GetFixedPoints[MutEquation[lambda],lambda],{lambda,\[Lambda]min,\[Lambda]max,step}],1]//Quiet;
stableToplot={#[[1]],#[[2]]}&/@Select[toplot,(#[[3]]==True)&];
unstableToplot={#[[1]],#[[2]]}&/@Select[toplot,((#[[3]]==False))&];
{Select[unstableToplot,#[[2]] >=  0&],Select[stableToplot,#[[2]]>=  0&]}
]
PlotFixedPointsMut[]:= PlotFixedPointsMut[GameName,0.01,2,00.01];
PlotFixedPointsMut[name_]:= PlotFixedPointsMut[name,0.01,2,00.01];
PlotFixedPointsMut[name_,\[Lambda]min_,\[Lambda]max_,step_]:=
Module[{toplot,stableToplot,unstableToplot,plot,testfunc},
toplot=Flatten[Table[GetFixedPoints[lhsmut[lambda],lambda],{lambda,\[Lambda]min,\[Lambda]max,step}],1]//Quiet;
stableToplot={#[[1]],#[[2]]}&/@Select[toplot,(#[[3]]==True)&];
unstableToplot={#[[1]],#[[2]]}&/@Select[toplot,((#[[3]]==False))&];
testfunc=True&(*#[[2]] >-0.000001&*);
plot =ListPlot[{Select[unstableToplot,testfunc],Select[stableToplot,testfunc]},PlotStyle-> {Blue,Red},AxesLabel-> {\[Lambda],"fixed point"},PlotLegends-> {"unstable","stable"},PlotLabel->Game,PlotRange-> {0,1}];
Show[plot]
];

(*Plotting the fixed points and a heatmap of the finite time quantities*)
HeatMapFpointsScAux[\[Lambda]_?NumericQ,\[Beta]_?NumericQ,a_?NumericQ,b_?NumericQ,c_?NumericQ,d_?NumericQ,Npl_?NumericQ,fonc_]:= 
Module[{tab1,toplot},
tab1={\[Lambda],#}&/@(fonc@@ {\[Beta],a,b,c,d,Npl,SCFoncP[\[Lambda]],SCFoncM[\[Lambda]],Tip,Tim});
toplot=Table[{tab1[[i]][[1]],(i)/Npl//N,tab1[[i]][[2]]},{i,1,Npl}]
];
HeatMapFpointsSc[\[Beta]_?NumericQ,a_?NumericQ,b_?NumericQ,c_?NumericQ,d_?NumericQ,Npl_?NumericQ,fpsc_,fonc_]:= 
Module[{plotarg,heatmap},
plotarg = Join@@Table[HeatMapFpointsScAux[\[Lambda],\[Beta],a,b,c,d,Npl,fonc],{\[Lambda],0,1,0.01}];
heatmap = ListContourPlot[plotarg,PlotLegends->Automatic,InterpolationOrder-> 0,PlotLabel-> FoncName<> " in a " <>GameName,PerformanceGoal-> QuantityUnit,PlotRange-> Full,FrameLabel-> {"\[Lambda]","\!\(\*SubscriptBox[\(x\), \(in\)]\)"}];
Show[heatmap,fpsc]
];
HeatMapFpointsSc[\[Beta]_?NumericQ,Npl_?NumericQ,fonc_]:=HeatMapFpointsSc[\[Beta],a,b,c,d,Npl,PlotFixedPointsSc[],fonc];
HeatMapFpointsSc[\[Beta]_?NumericQ,Npl_?NumericQ,fpsc_,fonc_]:=HeatMapFpointsSc[\[Beta],a,b,c,d,Npl,fpsc,fonc];

FlowDiag1D[func_]:=
Module[{objleft,objright,epsilon,der,px0,p0,p1,x0},
x0 = x/.FindRoot[func,{x,0.5}];
der = D[func,var];
epsilon=0.01;
If[Re[(der/.{x-> x0})]<0,px0={PointSize[Large],Red,Point[{x0,0}]},px0={PointSize[Large],Blue,Point[{x0,0}]}]
If[(der/.{x-> epsilon})//Re<0,p0={PointSize[Large],Red,Point[{0,0}]},p0={PointSize[Large],Blue,Point[{0,0}]}]
If[(der/.{x-> 1-epsilon})//Re<0,p1={PointSize[Large],Red,Point[{1,0}]},p1={PointSize[Large],Blue,Point[{1,0}]}]
If[(func/.{x-> x0/2})//Re<0 ,
objleft=Arrow[{{x0,0},{0,0.}}];
,
objleft=Arrow[{{0,0},{x0,0.}}];
];
If[(func/.{x-> x0+(1-x0)/2})//Re<0 ,
objright=Arrow[{{1,0},{x0,0.}}];
,
objright=Arrow[{{x0,0},{1,0.}}];
];
Show[Plot[func,{x,0,1}],Graphics[{objleft,objright,px0,p0,p1}]]
];
End[]
EndPackage[]















