(* Content-type: application/vnd.wolfram.mathematica *)

(*** Wolfram Notebook File ***)
(* http://www.wolfram.com/nb *)

(* CreatedBy='Mathematica 10.0' *)

(*CacheID: 234*)
(* Internal cache information:
NotebookFileLineBreakTest
NotebookFileLineBreakTest
NotebookDataPosition[       158,          7]
NotebookDataLength[     12433,        368]
NotebookOptionsPosition[     11391,        329]
NotebookOutlinePosition[     11727,        344]
CellTagsIndexPosition[     11684,        341]
WindowFrame->Normal*)

(* Beginning of Notebook Content *)
Notebook[{

Cell[CellGroupData[{
Cell["Computing the first passage time in 1D", "Title",
 CellChangeTimes->{{3.644839626002232*^9, 3.644839638953961*^9}}],

Cell[BoxData[{
 RowBox[{
  RowBox[{"DeltaPi", "=", 
   RowBox[{"Function", "[", 
    RowBox[{
     RowBox[{"{", 
      RowBox[{"a", ",", "b", ",", "c", ",", "d", ",", "i", ",", "Na"}], "}"}],
      ",", "\[IndentingNewLine]", 
     RowBox[{"Module", "[", 
      RowBox[{
       RowBox[{"{", 
        RowBox[{"u", ",", "v"}], "}"}], ",", "\[IndentingNewLine]", 
       RowBox[{
        RowBox[{"u", "=", 
         RowBox[{
          RowBox[{"(", 
           RowBox[{"a", "+", "d", "-", "b", "-", "c"}], ")"}], "/", 
          RowBox[{"(", 
           RowBox[{"Na", "-", "1"}], ")"}]}]}], ";", "\n", 
        RowBox[{"v", "=", 
         RowBox[{
          RowBox[{"(", 
           RowBox[{
            RowBox[{"Na", "*", 
             RowBox[{"(", 
              RowBox[{"b", "-", "d"}], ")"}]}], "-", 
            RowBox[{"(", 
             RowBox[{"a", "-", "d"}], ")"}]}], ")"}], "/", 
          RowBox[{"(", 
           RowBox[{"Na", "-", "1"}], ")"}]}]}], ";", "\n", 
        RowBox[{
         RowBox[{"u", "*", "i"}], "+", "v"}]}]}], "]"}]}], 
    "\[IndentingNewLine]", "]"}]}], ";"}], "\[IndentingNewLine]", 
 RowBox[{"DomGame", ":=", 
  RowBox[{"(", 
   RowBox[{
    RowBox[{"Clear", "[", 
     RowBox[{"a", ",", "b", ",", "c", ",", "d"}], "]"}], ";", 
    RowBox[{"a", "=", "5"}], ";", 
    RowBox[{"b", "=", "2"}], ";", 
    RowBox[{"c", "=", "3"}], ";", 
    RowBox[{"d", "=", "1"}], ";"}], ")"}]}], "\[IndentingNewLine]", 
 RowBox[{"CoexGame", ":=", 
  RowBox[{"(", 
   RowBox[{
    RowBox[{"Clear", "[", 
     RowBox[{"a", ",", "b", ",", "c", ",", "d"}], "]"}], ";", 
    RowBox[{"a", "=", "3"}], ";", 
    RowBox[{"b", "=", "2"}], ";", 
    RowBox[{"c", "=", "5"}], ";", 
    RowBox[{"d", "=", "1"}], ";"}], ")"}]}], "\[IndentingNewLine]", 
 RowBox[{"CoordGame", ":=", 
  RowBox[{"(", 
   RowBox[{
    RowBox[{"Clear", "[", 
     RowBox[{"a", ",", "b", ",", "c", ",", "d"}], "]"}], ";", 
    RowBox[{"a", "=", "5"}], ";", 
    RowBox[{"b", "=", "1"}], ";", 
    RowBox[{"c", "=", "3"}], ";", 
    RowBox[{"d", "=", "2"}], ";"}], ")"}]}]}], "Input",
 CellChangeTimes->{{3.644839814857911*^9, 3.6448398342660723`*^9}, {
  3.6448398669220037`*^9, 3.644839870544722*^9}, {3.644839951298943*^9, 
  3.644839953000119*^9}}],

Cell[BoxData[{
 RowBox[{
  RowBox[{"Clear", "[", 
   RowBox[{"a", ",", "b", ",", "c", ",", "d"}], "]"}], 
  ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"Limit", "[", 
   RowBox[{
    RowBox[{
     RowBox[{"DeltaPi", "[", 
      RowBox[{"a", ",", "b", ",", "c", ",", "d", ",", "i", ",", "Na"}], "]"}],
      "/.", 
     RowBox[{"{", 
      RowBox[{"i", "\[Rule]", " ", 
       RowBox[{"Na", "/", "x"}]}], "}"}]}], ",", 
    RowBox[{"Na", "\[Rule]", " ", "Infinity"}]}], "]"}], "//", 
  "Simplify"}]}], "Input",
 CellChangeTimes->{{3.644839954973569*^9, 3.644840027364585*^9}}],

Cell[BoxData[
 RowBox[{
  RowBox[{"DeltaPiLim", "[", 
   RowBox[{"a_", ",", "b_", ",", "c_", ",", "d_", ",", "x_"}], "]"}], ":=", 
  FractionBox[
   RowBox[{"a", "-", "c", "+", "d", "+", 
    RowBox[{"b", " ", 
     RowBox[{"(", 
      RowBox[{
       RowBox[{"-", "1"}], "+", "x"}], ")"}]}], "-", 
    RowBox[{"d", " ", "x"}]}], "x"]}]], "Input",
 CellChangeTimes->{{3.644840046263028*^9, 3.644840063880693*^9}}],

Cell[BoxData["\[IndentingNewLine]"], "Input",
 CellChangeTimes->{3.644839876831636*^9}],

Cell[CellGroupData[{

Cell["Computing the Fokker-Planck equation", "Chapter",
 CellChangeTimes->{{3.644839658793397*^9, 3.644839669354106*^9}}],

Cell[CellGroupData[{

Cell[BoxData[{"CoexGame", "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{
   RowBox[{"M1", "[", "x_", "]"}], " ", ":=", " ", 
   RowBox[{"x", "*", 
    RowBox[{"(", 
     RowBox[{"1", "-", "x"}], ")"}], "*", 
    RowBox[{"DeltaPiLim", "[", 
     RowBox[{"a", ",", "b", ",", "c", ",", "d", ",", "x"}], "]"}]}]}], 
  ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{
   RowBox[{"M2", "[", "x_", "]"}], ":=", 
   RowBox[{"x", "*", 
    RowBox[{"(", 
     RowBox[{"1", "-", "x"}], ")"}]}]}], " "}], "\[IndentingNewLine]", 
 RowBox[{"beta", " ", "=", " ", "0.1"}]}], "Input",
 CellChangeTimes->{{3.6448396705943193`*^9, 3.644839687948142*^9}, {
  3.644839890356372*^9, 3.644839947308292*^9}, {3.64484006970533*^9, 
  3.644840097857986*^9}, {3.644840205559621*^9, 3.644840205966447*^9}, {
  3.644840413519907*^9, 3.6448404269509687`*^9}, {3.644840478561413*^9, 
  3.644840481505844*^9}}],

Cell[BoxData["0.1`"], "Output",
 CellChangeTimes->{3.644840427489236*^9, 3.6448404818149853`*^9}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"eq", "=", 
  RowBox[{"{", 
   RowBox[{
    RowBox[{"0", " ", "\[Equal]", "  ", 
     RowBox[{
      RowBox[{
       RowBox[{"D", "[", 
        RowBox[{
         RowBox[{"M1", "[", "x", "]"}], ",", "x"}], "]"}], " ", 
       RowBox[{"D", "[", 
        RowBox[{
         RowBox[{"T1", "[", "x", "]"}], ",", "x"}], "]"}]}], "+", 
      RowBox[{
       RowBox[{"0.5", "/", "beta"}], "*", 
       RowBox[{"M2", "[", "x", "]"}], "*", 
       RowBox[{"D", "[", 
        RowBox[{
         RowBox[{"T1", "[", "x", "]"}], ",", "x", ",", "x"}], "]"}]}]}]}], 
    ",", 
    RowBox[{
     RowBox[{"T1", "[", "0.", "]"}], "\[Equal]", "0."}], ",", 
    RowBox[{
     RowBox[{"T1", "[", "1", "]"}], "\[Equal]", "0."}]}], "}"}], 
  " "}]], "Input",
 CellChangeTimes->{{3.64484011197113*^9, 3.644840148662427*^9}, {
  3.6448401879742403`*^9, 3.644840281516408*^9}, {3.644840347859025*^9, 
  3.644840403705658*^9}, {3.64484043804984*^9, 3.6448404381543922`*^9}, {
  3.644840489568983*^9, 3.644840492640736*^9}, {3.644840621219446*^9, 
  3.644840621490376*^9}, {3.644840672114975*^9, 3.644840687596568*^9}}],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{
   RowBox[{"0", "\[Equal]", 
    RowBox[{
     RowBox[{
      RowBox[{"(", 
       RowBox[{"2", "-", 
        RowBox[{"2", " ", 
         RowBox[{"(", 
          RowBox[{
           RowBox[{"-", "1"}], "+", "x"}], ")"}]}]}], ")"}], " ", 
      RowBox[{
       SuperscriptBox["T1", "\[Prime]",
        MultilineFunction->None], "[", "x", "]"}]}], "+", 
     RowBox[{"5.`", " ", 
      RowBox[{"(", 
       RowBox[{"1", "-", "x"}], ")"}], " ", "x", " ", 
      RowBox[{
       SuperscriptBox["T1", "\[Prime]\[Prime]",
        MultilineFunction->None], "[", "x", "]"}]}]}]}], ",", 
   RowBox[{
    RowBox[{"T1", "[", "0.`", "]"}], "\[Equal]", "0.`"}], ",", 
   RowBox[{
    RowBox[{"T1", "[", "1", "]"}], "\[Equal]", "0.`"}]}], "}"}]], "Output",
 CellChangeTimes->{{3.644840200272703*^9, 3.64484021693161*^9}, 
   3.644840270144897*^9, 3.644840327649662*^9, {3.644840361610632*^9, 
   3.644840438454715*^9}, {3.644840484420372*^9, 3.6448404932713833`*^9}, 
   3.644840688159027*^9}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"NDSolve", "[", 
  RowBox[{"eq", ",", 
   RowBox[{"T1", "[", "x", "]"}], ",", "x"}], "]"}]], "Input",
 CellChangeTimes->{{3.644840273232588*^9, 3.644840343210144*^9}}],

Cell[BoxData[
 RowBox[{
  StyleBox[
   RowBox[{"Power", "::", "infy"}], "MessageName"], 
  RowBox[{
  ":", " "}], "\<\"Infinite expression \[NoBreak]\\!\\(1\\/0.`\\)\[NoBreak] \
encountered. \\!\\(\\*ButtonBox[\\\"\[RightSkeleton]\\\", \
ButtonStyle->\\\"Link\\\", ButtonFrame->None, \
ButtonData:>\\\"paclet:ref/message/General/infy\\\", ButtonNote -> \
\\\"Power::infy\\\"]\\)\"\>"}]], "Message", "MSG",
 CellChangeTimes->{{3.644840366448443*^9, 3.644840441027193*^9}, 
   3.644840495095029*^9, 3.644840690915349*^9}],

Cell[BoxData[
 RowBox[{
  StyleBox[
   RowBox[{"Infinity", "::", "indet"}], "MessageName"], 
  RowBox[{
  ":", " "}], "\<\"Indeterminate expression \[NoBreak]\\!\\(0.`\\\\ \
ComplexInfinity\\)\[NoBreak] encountered. \\!\\(\\*ButtonBox[\\\"\
\[RightSkeleton]\\\", ButtonStyle->\\\"Link\\\", ButtonFrame->None, \
ButtonData:>\\\"paclet:ref/message/General/indet\\\", ButtonNote -> \
\\\"Infinity::indet\\\"]\\)\"\>"}]], "Message", "MSG",
 CellChangeTimes->{{3.644840366448443*^9, 3.644840441027193*^9}, 
   3.644840495095029*^9, 3.644840690966242*^9}],

Cell[BoxData[
 RowBox[{
  StyleBox[
   RowBox[{"Power", "::", "infy"}], "MessageName"], 
  RowBox[{
  ":", " "}], "\<\"Infinite expression \[NoBreak]\\!\\(1\\/0.`\\)\[NoBreak] \
encountered. \\!\\(\\*ButtonBox[\\\"\[RightSkeleton]\\\", \
ButtonStyle->\\\"Link\\\", ButtonFrame->None, \
ButtonData:>\\\"paclet:ref/message/General/infy\\\", ButtonNote -> \
\\\"Power::infy\\\"]\\)\"\>"}]], "Message", "MSG",
 CellChangeTimes->{{3.644840366448443*^9, 3.644840441027193*^9}, 
   3.644840495095029*^9, 3.6448406910134687`*^9}],

Cell[BoxData[
 RowBox[{
  StyleBox[
   RowBox[{"Infinity", "::", "indet"}], "MessageName"], 
  RowBox[{
  ":", " "}], "\<\"Indeterminate expression \[NoBreak]\\!\\(0.`\\\\ \
ComplexInfinity\\)\[NoBreak] encountered. \\!\\(\\*ButtonBox[\\\"\
\[RightSkeleton]\\\", ButtonStyle->\\\"Link\\\", ButtonFrame->None, \
ButtonData:>\\\"paclet:ref/message/General/indet\\\", ButtonNote -> \
\\\"Infinity::indet\\\"]\\)\"\>"}]], "Message", "MSG",
 CellChangeTimes->{{3.644840366448443*^9, 3.644840441027193*^9}, 
   3.644840495095029*^9, 3.6448406910645647`*^9}],

Cell[BoxData[
 RowBox[{
  StyleBox[
   RowBox[{"NDSolve", "::", "ndnum"}], "MessageName"], 
  RowBox[{
  ":", " "}], "\<\"Encountered non-numerical value for a derivative at \
\[NoBreak]\\!\\(x\\)\[NoBreak] == \[NoBreak]\\!\\(0.`\\)\[NoBreak]. \
\\!\\(\\*ButtonBox[\\\"\[RightSkeleton]\\\", ButtonStyle->\\\"Link\\\", \
ButtonFrame->None, ButtonData:>\\\"paclet:ref/message/NDSolve/ndnum\\\", \
ButtonNote -> \\\"NDSolve::ndnum\\\"]\\)\"\>"}]], "Message", "MSG",
 CellChangeTimes->{{3.644840366448443*^9, 3.644840441027193*^9}, 
   3.644840495095029*^9, 3.64484069109009*^9}],

Cell[BoxData[
 RowBox[{"NDSolve", "[", 
  RowBox[{
   RowBox[{"{", 
    RowBox[{
     RowBox[{"0", "\[Equal]", 
      RowBox[{
       RowBox[{
        RowBox[{"(", 
         RowBox[{"2", "-", 
          RowBox[{"2", " ", 
           RowBox[{"(", 
            RowBox[{
             RowBox[{"-", "1"}], "+", "x"}], ")"}]}]}], ")"}], " ", 
        RowBox[{
         SuperscriptBox["T1", "\[Prime]",
          MultilineFunction->None], "[", "x", "]"}]}], "+", 
       RowBox[{"5.`", " ", 
        RowBox[{"(", 
         RowBox[{"1", "-", "x"}], ")"}], " ", "x", " ", 
        RowBox[{
         SuperscriptBox["T1", "\[Prime]\[Prime]",
          MultilineFunction->None], "[", "x", "]"}]}]}]}], ",", 
     RowBox[{
      RowBox[{"T1", "[", "0.`", "]"}], "\[Equal]", "0.`"}], ",", 
     RowBox[{
      RowBox[{"T1", "[", "1", "]"}], "\[Equal]", "0.`"}]}], "}"}], ",", 
   RowBox[{"T1", "[", "x", "]"}], ",", "x"}], "]"}]], "Output",
 CellChangeTimes->{{3.6448402925924883`*^9, 3.6448404410483522`*^9}, 
   3.6448404952201643`*^9, 3.644840691091441*^9}]
}, Open  ]]
}, Open  ]]
}, Open  ]]
},
WindowSize->{960, 1138},
WindowMargins->{{0, Automatic}, {Automatic, 0}},
FrontEndVersion->"10.0 for Linux x86 (64-bit) (December 4, 2014)",
StyleDefinitions->"Default.nb"
]
(* End of Notebook Content *)

(* Internal cache information *)
(*CellTagsOutline
CellTagsIndex->{}
*)
(*CellTagsIndex
CellTagsIndex->{}
*)
(*NotebookFileOutline
Notebook[{
Cell[CellGroupData[{
Cell[580, 22, 121, 1, 96, "Title"],
Cell[704, 25, 2229, 62, 297, "Input"],
Cell[2936, 89, 582, 17, 55, "Input"],
Cell[3521, 108, 413, 11, 63, InheritFromParent],
Cell[3937, 121, 87, 1, 55, "Input"],
Cell[CellGroupData[{
Cell[4049, 126, 121, 1, 69, "Chapter"],
Cell[CellGroupData[{
Cell[4195, 131, 880, 21, 99, "Input"],
Cell[5078, 154, 97, 1, 32, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[5212, 160, 1111, 29, 32, "Input"],
Cell[6326, 191, 1019, 28, 32, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[7382, 224, 190, 4, 32, "Input"],
Cell[7575, 230, 519, 11, 49, "Message"],
Cell[8097, 243, 549, 11, 23, "Message"],
Cell[8649, 256, 521, 11, 49, "Message"],
Cell[9173, 269, 551, 11, 23, "Message"],
Cell[9727, 282, 575, 11, 23, "Message"],
Cell[10305, 295, 1046, 29, 32, "Output"]
}, Open  ]]
}, Open  ]]
}, Open  ]]
}
]
*)

(* End of internal cache information *)

