(* ::Package:: *)

BeginPackage["AssymGames`"]
(*Documentation of the package*)
BSCycles::usage = "BSCycle set the coefficients matrix for a cyclic bos game"
BSMixed::usage = "BSCycle set the coefficients matrix for a mixed strategies bos game"
BSPure::usage = "BSCycle set the coefficients matrix for a pure strategies bos game"
T1p::usage = "T1p[i,j,Npl,beta]Transition rate from state (i,j) to state (i+1,j) when there is Npl players for some beta as inverse temperature"
T1m::usage = "T1m[i,j,Npl,beta]Transition rate from state (i,j) to state (i-1,j) when there is Npl players for some beta as inverse temperature"
T2p::usage = "T2p[i,j,Npl,beta]Transition rate from state (i,j) to state (i,j+1) when there is Npl players for some beta as inverse temperature"
T2m::usage = "T2m[i,j,Npl,beta]Transition rate from state (i,j) to state (i,j-1) when there is Npl players for some beta as inverse temperature"
PayoffA1::usage= "PayoffA1[i,Npl] Payoff of a player from population 1 doing action A"
PayoffB1::usage= "PayoffB1[i,Npl] Payoff of a player from population 1 doing action B"
PayoffA2::usage= "PayoffA2[j,Npl] Payoff of a player from population 2 doing action A"
PayoffB2::usage= "PayoffB2[j,Npl] Payoff of a player from population 2 doing action B"
SwitchProb::usage = "SwitchProb[Pf,Pi,\[Beta]] switching probabilty from state with Payoff Pi to state with Payoff Pf and intensity of choice \[Beta]"
QMatElement::usage = "QMatElement[xf_,xi_,Npl_,beta_] gives the element (xi,xf) of the Q-matrix"
GenerateQmat::usage = "GenerateQmat[\[Beta],Npl,QMatElement] Generate the Q-matrix for Npl players and \[Beta] as an intensity of choice parameter"
GetFprob::usage = "GetFprob[\[Beta]_,Npl_,S_] compute the Fixation probability matrix. S = {a11,a1N,aN1,aNN} where aij is equal to 1 if you wnat to compute the probability to fix in this state and 0 else"
GetUcFtime::usage = "GetUcFtime[\[Beta]_,Npl_] compute the unconditional fixation time"
PlotQt::usage = "Plot finite size quantities. Takes as an argument the output of GetFprob or GetUcFtime"
CondQMatElement::usage = "CondQMatElement[xf_,xi_,Npl_,beta_,fprob_] elements of the markov chain modified to have fixation on in the defined by the function fprob"
GenerateCondQmat::usage = "GenerateCondQmat[\[Beta]_,Npl_,S_] Generate the matrix of the conditional markov chain with conditional ixation in the state defined by S"
GetCFtime::usage = "GetCFtime[\[Beta],Npl,S] compute the conditional fixation time in the set S"
CustomGameSym::usage = "CustomGame[a_,b_,c_,d_,e_,f_,g_,h_] create a custom game"
PlotStability::usage ="PlotStability[\[Lambda]] will compute the diagramm of the fixd points of the dynamic"
lhs1::usage = "first replicator equation";
lhs2::usage = "second replicator equation";
BSMixed[eps_]:={a1,b1,c1,d1,a2,b2,c2,d2}={0,1,1-eps,0,0,1,1,0}
Begin["`Private`"]
(*SwitchProb[Pf_,Pi_,\[Beta]_]:=(1/(1+Exp[-\[Beta](Pf-Pi)]));*)
SwitchProb[Pf_,Pi_,\[Beta]_]:=(0.5*(1+\[Beta]/2(Pf-Pi)));
BSCycles:={a1,b1,c1,d1,a2,b2,c2,d2}={1,-1,-1,1,-1,1,1,-1}(*ok*)
CustomGameSym[a_,b_,c_,d_,e_,f_,g_,h_]:={a1,b1,c1,d1,a2,b2,c2,d2}={a,b,c,d,e,f,g,h}(*ok*)
BSMixed:={a1,b1,c1,d1,a2,b2,c2,d2}={0,2,3,0,0,5,1,0}(*ok*)(*ok*)
BSMixed[eps_]:={a1,b1,c1,d1,a2,b2,c2,d2}={0,1,1-eps,0,0,1,1,0}
BSPure:={a1,b1,c1,d1,a2,b2,c2,d2}={0,-1,1,0,0,1,1,0}(*ok*)
(*functions which describe the dynamic*)
T1p[i_,j_,Npl_,beta_]:= i*(Npl-i)/Npl*SwitchProb[PayoffA1[j,Npl],PayoffB1[j,Npl],beta];
T1m[i_,j_,Npl_,beta_]:= i*(Npl-i)/Npl*SwitchProb[PayoffB1[j,Npl],PayoffA1[j,Npl],beta];
T2p[i_,j_,Npl_,beta_]:= j*(Npl-j)/Npl*SwitchProb[PayoffA2[i,Npl],PayoffB2[i,Npl],beta];
T2m[i_,j_,Npl_,beta_]:= j*(Npl-j)/Npl*SwitchProb[PayoffB2[i,Npl],PayoffA2[i,Npl],beta];
PayoffA1[j_,Npl_] := j/Npl * a1 + (Npl-j)/Npl * b1;
PayoffB1[j_,Npl_] := j/Npl * c1 + (Npl-j)/Npl * d1;
PayoffA2[i_,Npl_] := i/Npl * a2 + (Npl-i)/Npl * b2;
PayoffB2[i_,Npl_] := i/Npl * c2 + (Npl-i)/Npl * d2;

(**************************)
(*Generation of the Matrix*)
(**************************)
QMatElement[xf_,xi_,Npl_,beta_]:=
Module[{output=0},
If[xf=== xi,output = 0];
If[(xf[[1]]-xi[[1]]==1)&&(xf[[2]]==xi[[2]]),output=T1p[xi[[1]],xi[[2]],Npl,beta]];
If[(xf[[1]]-xi[[1]]==-1)&&(xf[[2]]==xi[[2]]),output=T1m[xi[[1]],xi[[2]],Npl,beta]];
If[(xf[[2]]-xi[[2]]==1)&&(xf[[1]]==xi[[1]]),output=T2p[xi[[1]],xi[[2]],Npl,beta]];
If[(xf[[2]]-xi[[2]]==-1)&&(xf[[1]]==xi[[1]]),output=T2m[xi[[1]],xi[[2]],Npl,beta]];
output
];
(*Element of the conditional matrix*)
CondQMatElement[xf_,xi_,Npl_,beta_,fprob_]:=
Module[{output=0,ratio =0},
If[ fprob[[#1+1,#2+1]]&@@xi!=   0,ratio=(fprob[[#1+1,#2+1]]&@@xf)/(fprob[[#1+1,#2+1]]&@@xi);];
If[xf=== xi,output = 0];
If[(xf[[1]]-xi[[1]]==1)&&(xf[[2]]==xi[[2]]),output=T1p[xi[[1]],xi[[2]],Npl,beta]];
If[(xf[[1]]-xi[[1]]==-1)&&(xf[[2]]==xi[[2]]),output=T1m[xi[[1]],xi[[2]],Npl,beta]];
If[(xf[[2]]-xi[[2]]==1)&&(xf[[1]]==xi[[1]]),output=T2p[xi[[1]],xi[[2]],Npl,beta]];
If[(xf[[2]]-xi[[2]]==-1)&&(xf[[1]]==xi[[1]]),output=T2m[xi[[1]],xi[[2]],Npl,beta]];
output*ratio
];
GenerateQmat[\[Beta]_,Npl_,QMatElement_]:=
Module[{MatCoord,Qmat,GetDiagTerm,lli},
MatCoord:=Table[{i,j},{i,0,Npl},{j,0,Npl}]//Flatten[#,1]&;
Qmat = Table[QMatElement[MatCoord[[j]],MatCoord[[i]],Npl,\[Beta]],{i,1,MatCoord//Length},{j,1,MatCoord//Length}];
GetDiagTerm[Mat_,li_]:=-Plus@@Mat[[li]];
For[lli=1,lli<= Length[MatCoord],lli++,
Qmat[[lli,lli]]=GetDiagTerm[Qmat,lli];
];
{Qmat,MatCoord}
];
GenerateQmat[\[Beta]_,Npl_]:=GenerateQmat[\[Beta],Npl,QMatElement];
GenerateCondQmat[\[Beta]_,Npl_,S_]:= 
Module[{fprob = GetFprob[\[Beta],Npl,S]},
GenerateQmat[\[Beta],Npl,CondQMatElement[#1,#2,#3,#4,fprob]&]
]
(*********************************************)
(*Getting the finite size dynamic quantities*)
(*********************************************)
GetFprob[\[Beta]_,Npl_,S_]:=
Module[{GQmat,vars,Qmat,eqs,incond,withincond,nvars,sol},
GQmat = GenerateQmat[\[Beta],Npl];
vars=Subscript[h,#]&/@GQmat[[2]];
Qmat=GQmat[[1]];
eqs=Qmat.vars;
incond={Subscript[h,{0,0}]-> S[[1]],Subscript[h,{0,Npl}]-> S[[2]],Subscript[h,{Npl,Npl}]-> S[[3]],Subscript[h,{Npl,0}]-> S[[4]]};
withincond = eqs/.incond;
nvars = vars/.incond;
nvars=DeleteCases[nvars,_Integer];
withincond =(#==0&/@withincond);
withincond;
sol=Solve[withincond,nvars];
Flatten[Table[Subscript[h,{i,j}],{i,0,Npl},{j,0,Npl}]/.sol/.incond,1]
];
GetUcFtime[\[Beta]_,Npl_,GQmat_]:=
Module[{Qmat,vars,eqs,incond,withincond,nvars,sol,cond},
Qmat=GQmat[[1]];
vars=Subscript[h,#]&/@GQmat[[2]];
eqs=Qmat.vars;
incond={Subscript[h,{0,0}]-> 0,Subscript[h,{0,Npl}]->0,Subscript[h,{Npl,Npl}]-> 0,Subscript[h,{Npl,0}]-> 0};
withincond = eqs/.incond;
nvars = vars/.incond;
nvars=DeleteCases[nvars,_Integer];
(*cond=#>= 0&/@nvars;*)
withincond =(#==-1&/@withincond);
sol=Solve[DeleteCases[(*Join[*)withincond(*,cond]*),x_/;Not[x]],nvars];
Flatten[Table[Subscript[h,{i,j}],{i,0,Npl},{j,0,Npl}]/.sol/.incond,1]
];
GetUcFtime[\[Beta]_,Npl_]:=GetUcFtime[\[Beta],Npl, GenerateQmat[\[Beta],Npl]]
GetCFtime[\[Beta]_,Npl_,S_]:=GetUcFtime[\[Beta],Npl,GenerateCondQmat[\[Beta],Npl,S]]
(**** Plot functions ****)
PlotQt[x_]:=ArrayPlot[x,PlotLegends-> Automatic,FrameTicks->Automatic,ColorFunction->"Rainbow",FrameLabel-> {"x","y"}];
(*Plotting the stability diagram for the standard dynamic*)
(*defininft he payoff*)
PayoffCA1[x_,y_,\[Lambda]_] := y * a1 + (1-y)* b1-\[Lambda]*Log[x];
PayoffCB1[x_,y_,\[Lambda]_] := y * c1 + (1-y) * d1-\[Lambda]*Log[1-x];
PayoffCA2[x_,y_,\[Lambda]_] :=x * a2 + (1-x) * b2-\[Lambda]*Log[y];
PayoffCB2[x_,y_,\[Lambda]_] := x * c2 + (1-x)* d2-\[Lambda]*Log[1-y];
lhs1[\[Lambda]_]:= x*(PayoffCA1[x,y,\[Lambda]] -(x*PayoffCA1[x,y,\[Lambda]] +(1-x)*PayoffCB1[x,y,\[Lambda]] ))
lhs2[\[Lambda]_]:= y*(PayoffCA2[x,y,\[Lambda]] -(y*PayoffCA2[x,y,\[Lambda]] +(1-y)*PayoffCB2[x,y,\[Lambda]] ))
lhs1[\[Lambda]_,x_,y_]:= x*(PayoffCA1[x,y,\[Lambda]] -(x*PayoffCA1[x,y,\[Lambda]] +(1-x)*PayoffCB1[x,y,\[Lambda]] ))
lhs2[\[Lambda]_,x_,y_]:= y*(PayoffCA2[x,y,\[Lambda]] -(y*PayoffCA2[x,y,\[Lambda]] +(1-y)*PayoffCB2[x,y,\[Lambda]] ))
PlotStability[0]:=
Module[{Jac,sol,jacob,ev,fintab,stab,unstab,cycle,\[Lambda]=0},
Jac:={D[{lhs1[\[Lambda]],lhs2[\[Lambda]]},x],D[{lhs1[\[Lambda]],lhs2[\[Lambda]]},y]};
sol=Solve[{lhs1[\[Lambda]]==0 ,lhs2[\[Lambda]]==0},{x,y}];
jacob=Jac/.sol;
ev=Max[Re/@Eigenvalues[#]]&/@jacob;
fintab=Table[{ev[[i]]}~Join~{x,y}/.sol[[i]],{i,1,Length[sol]}];
stab={#[[2]],#[[3]]}&/@Select[fintab,Re[#[[1]]]<0&];
unstab={#[[2]],#[[3]]}&/@Select[fintab,Re[#[[1]]]>0&];
cycle={#[[2]],#[[3]]}&/@Select[fintab,Re[#[[1]]]== 0&];
Show@@{ListPlot[unstab,PlotStyle-> Black,PlotLegends->LineLegend[{Black,Red,Pink},{"unstable","stable","cycles"}]],ListPlot[stab,PlotStyle-> Red],ListPlot[cycle,PlotStyle-> Pink],StreamPlot[{lhs1[\[Lambda]],lhs2[\[Lambda]]},{x,0,1},{y,0,1}]}
];
PlotStability[x_,b_]:=
Module[{Jac,sol,jacob,ev,fintab,stab,unstab,cycle,\[Lambda]=0},
Jac:={D[{lhs1[\[Lambda]],lhs2[\[Lambda]]},x],D[{lhs1[\[Lambda]],lhs2[\[Lambda]]},y]};
sol=Solve[{lhs1[\[Lambda]]==0 ,lhs2[\[Lambda]]==0},{x,y}];
jacob=Jac/.sol;
ev=Max[Re/@Eigenvalues[#]]&/@jacob;
fintab=Table[{ev[[i]]}~Join~{x,y}/.sol[[i]],{i,1,Length[sol]}];
stab={#[[2]],#[[3]]}&/@Select[fintab,Re[#[[1]]]<0&];
unstab={#[[2]],#[[3]]}&/@Select[fintab,Re[#[[1]]]>0&];
cycle={#[[2]],#[[3]]}&/@Select[fintab,Re[#[[1]]]== 0&];
Show@@{ListPlot[unstab,PlotStyle-> Black,{x,0,1},{y,0,1}]}
];
PlotFixedPoints[\[Lambda]_,arg___]:=
Module[{Jac,sol,jacob,ev,fintab,stab,unstab,cycle,sp,step=0.1,\[Epsilon]=0.1},
Jac:={D[{lhs1[\[Lambda],x,y],lhs2[\[Lambda],x,y]},x],D[{lhs1[\[Lambda],x,y],lhs2[\[Lambda],x,y]},y]};
sol=FindRoot[{lhs1[\[Lambda],x,y]==0,lhs2[\[Lambda],x,y]==0},{{x,#[[1]]},{y,#[[2]]}}]&/@Flatten[Table[{i,j},{i,0.01,1,step},{j,0.01,1,step}],1];
sol=DeleteDuplicates[{Re[x],Re[y]}/.sol,Norm[#1-#2]<0.0001&];
sol={x-> #[[1]],y-> #[[2]]}&/@sol;
jacob=Jac/.sol;
ev=Max[Re/@Eigenvalues[#]]&/@jacob;
fintab=Table[{ev[[i]]}~Join~{x,y}/.sol[[i]],{i,1,Length[sol]}];
stab={#[[2]],#[[3]]}&/@Select[fintab,Re[#[[1]]]<0&];
unstab={#[[2]],#[[3]]}&/@Select[fintab,Re[#[[1]]]>0&];
cycle={#[[2]],#[[3]]}&/@Select[fintab,Re[#[[1]]]== 0&];
sp=arg//ListStreamPlot[Table[{{x,y},{lhs1[\[Lambda],x,y],lhs2[\[Lambda],x,y]}},{x,0.01,1,0.01},{y,0.01,1,0.01}],##]&;
Show@@{ListPlot[unstab,PlotStyle-> Black,PlotLegends->LineLegend[{Black,Red,Pink},{"unstable","stable","cycles"}]],ListPlot[stab,PlotStyle-> Red],ListPlot[cycle,PlotStyle-> Pink],sp(*StreamPlot[{lhs1[\[Lambda],x,y],lhs2[\[Lambda],x,y]},{x,\[Epsilon],1-\[Epsilon]},{y,\[Epsilon],1-\[Epsilon]}]*)}
(*arg//ListPlot[sol,##]&*)
]
PlotStability[\[Lambda]_,arg___]:=arg//PlotFixedPoints[\[Lambda],##]&
End[]
EndPackage[]

























