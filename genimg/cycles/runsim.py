import numpy as np
import subprocess as sp

## Simulations for the matching penny_game

def simu_mp_N():
	for Npl in np.arange(10,1000,50):
		i0 = Npl/2
		cmd = "./gillespie.out  --i0 " + str(Npl/2) + " --j0 " + str(Npl/2) + " --Np " + str(Npl)+ " --lambda 0 --o " + str(Npl) + ".dat --nsim 2000 --sc"  
		print(cmd)
		sp.call(cmd,shell = True)


		
def simu_mp_lambda(l = 0.3,fname = "cycle_lambda_03.dat"):
	fich = open(fname, "w")
	for Npl in np.arange(10,150,10):
		i0 = Npl/2
		cmd = "./gillespie.out  --i0 " + str(Npl/2) + " --j0 " + str(Npl/2) + " --Np " + str(Npl)+" --nsim 200 --lambda " + str(l) +" --sc --a1 1 --b1 -1 --c1 -1 --d1 1 --a2 -1 --b2 1 --c2 1 --d2 -1" 
		print(cmd)
		sp.call("pwd", shell = True)
		sp.call(cmd,shell = True)
		results = str(Npl) + "\t" + str(MeanUcFtime()) + "\n"
		print results 
		fich.write(results)	
	fich.close()
	return 1
		
def MeanUcFtime(fname = 'ftime.dat'):
	tmp = np.loadtxt(fname, skiprows = 1)
	return np.mean(tmp[:,1])

def BSDomlambda(beta  = 0.1, nsim = 500, fname = "BSDomlambda.txt", Npl = 100, shellv = False):
	fich = open(fname, "w")
	fich.close()
	for lambdav in np.arange(0.1,0.9,0.1):
		fich = open(fname, "a")
		cl = "./gillespie.out --beta "+str(beta)+" --lambda " + str(lambdav) +" --a1 0 --b1 -1 --c1 1 --d1 0 --a2 0 --b2 1 --c2 1 --d2 0 --Np "+ str(Npl) +" --nsim "+ str(nsim) +" --sc --i0 " + str(Npl/2) + " --j0 " + str(Npl/2)
		print cl
		sp.call(cl, shell = True)
		results = str(lambdav) + "\t" + str(MeanUcFtime()) + "\n"
		print results 
		fich.write(results)
		fich.close()
	return cl

def BSCoordLambda(beta  = 0.1, nsim = 200, fname = "BSCoordlambda.txt", Npl = 100, shellv = False):
	fich = open(fname, "w")
	fich.close()
	for lambdav in np.arange(0.1,1.8,0.1):
		fich = open(fname, "a")
		cl = "./gillespie.out --beta "+str(beta)+" --lambda " + str(lambdav) +" --a1 2 --b1 0 --c1 0 --d1 1 --a2 1 --b2 0 --c2 0 --d2 2 --Np "+ str(Npl) +" --nsim "+ str(nsim) +" --sc --i0 " + str(Npl/2) + " --j0 " + str(Npl/2)
		print cl
		sp.call(cl, shell = True)
		results = str(lambdav) + "\t" + str(MeanUcFtime()) + "\n"
		print results 
		fich.write(results)
		fich.close()
	return cl

def BSDomN(beta  = 0.1, nsim = 500, fname = "BSDomN.txt", lambdav = 0., shellv = False):
	fich = open(fname, "w")
	for Npl in np.arange(100,4000,100):
		fich = open(fname, "a")
		cl = "./gillespie.out --beta "+str(beta)+" --lambda " + str(lambdav) +" --a1 0 --b1 -1 --c1 1 --d1 0 --a2 0 --b2 1 --c2 1 --d2 0 --Np "+ str(Npl) +" --nsim "+ str(nsim) +" --sc --i0 " + str(Npl/2) + " --j0 " + str(Npl/2)
		print cl
		sp.call(cl, shell = True)
		results = str(Npl) + "\t" + str(MeanUcFtime()) + "\n"
		print results 
		fich.write(results)
	fich.close()
	return cl
