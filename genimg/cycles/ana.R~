### Battle of the sexes with cycles
require(ggpmisc)
library(ggplot2)
library(ggthemes)

l = dir(patt = "[0-9][.][0-9][.]dat")
l
lambda = as.numeric(gsub(".dat","",l))
getucftime <- function(fname = "toto.dat", toreplace = ".dat")
    {
        tab <- read.table(fname, header = T)
        c(as.numeric(sub(pattern = toreplace, replacement = "", x = fname)), mean(tab$time))
    }

thm <- theme_base() +theme(text=element_text(size=14),axis.text=element_text(size=16 ,family ="Palatino"),axis.title=element_text(size=30, family ="Palatino"))

## fixation time as a function of lambda
ucftime = apply(as.matrix(l),1,getucftime)
ucftime
dataf <- as.data.frame(t(ucftime))
colnames(dataf) <- c("lambda", "t")

cycles <- ggplot(dataf, aes(x = lambda, y = t)) +
    scale_y_log10(breaks = c(10^2,10^3,10^4,10^5)) +
    geom_point(size = 3)  + xlab(expression(lambda)) + ylab(expression(tau(50,50))) +
    thm
pdf("~/Images/bscycle2.pdf")
cycles
dev.off()

## Plotting the fixation time as a function of N
fnames <- dir(patt = "[0-9]{2,}.{1}dat")
Nlist <-as.numeric(sub(pattern = ".dat", replacement = "", x = fnames))
ucftime = apply(as.matrix(fnames),1,getucftime)
ucftime
dataf <- as.data.frame(t(ucftime))
my.formula <- t ~ N
fit <- lm(my.formula, data = dataf)
sum(fit$residuals^2)
colnames(dataf) <- c("N", "t")
pl2 <- ggplot(dataf, aes(x = N, y = t))  +
    geom_point(shape = 4, size = 3) +
    geom_smooth(method = 'lm', se = FALSE, color = "black", size = 0.3)+
    xlab(expression(N)) +
    ylab(expression(tau(N/2,N/2))) +
    theme_bw() +
    labs(title = expression(lambda ~ "=" ~ 0))
    theme(text=element_text(size=14),axis.text=element_text(size=16 ,family ="Palatino"),axis.title=element_text(size=20, family ="Palatino"))
