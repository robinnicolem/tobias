



1) I think the main message of the paper is the representation of SC learning as a finite population. While we should obviously present an analysis of the finite size birth death dynamics, the mathematical formalism itself used to analyse the outcome should be in the background. So I’d relegate some of the details to a supplement (we can do this at a later stage).

2) Generally I think the main focus should be the effects of lambda (memory loss) on fixation times. The dependence on N has been studied in numerous other models (unrelated to SC), so this is in some sense standard and not really new. I am not saying we should not have any of this, but I’d say the main focus should be the parameters intrinsic to SC (i.e. mainly lambda).

3) Going through the draft here a few more comments:

Eq. (3.26): the origins have to be explained in more detail. We could have an appendix containing the details of the KM expansion, and the derivation of the effective potential.

Figs 1 and 2: I am not sure I understand the insets. What does ‘Exp’ and `Log’ mean. Also the vertical axes of the insets are not labelled. What is the message of the insets and why do we need them here? A similar comment applies to later figures.

Figures general: please make sure all axes are labelled appropriately, simply writing x and y isn’t really very good (for example Fig 1)

Fig 2: I don’t think this shows the drift actually, it shows fixed points and their stability.

Fig 3: This is just a straight line. Why do we need this figure?  This can be described in the text, can’t it? Is this simulation data or from the theory. The inset is too small for me to see anything, I am afraid.

Fig 4: What is the green shading?

Generally: There is not much (not anything really) about fixation times in the section on symmetric games. The focus as it stands, is on the deterministic bifurcation diagrams of the SC equations, and how they are modified by finite N. But I think we have to say a lot more about the stochastic dynamics, and what it actually does. Measuring, calculating and showing fixation times and fixation probabilities should be the main focus of this section I’d say. This is what evolutionary modellers of finite populations will be interested in.

Similarly, in Sec 4 there is very little (nothing?) about the dynamics in finite populations, and on fixation times/probabilities. Again I’d say this should be the main focus. The deterministic bifurcations are probably needed to explain/comment on the finitite-N dynamics, but they are merely a tool, not the main message (the deterministic flow diagrams can be obtained without ever introducing the population of ideas).

Again, I don’t think Table 4 can be the main result of our study. We can keep it, as it is interesting. This is known in other contexts before (e.g. stable fixed point means exponential fixation time in N). You have very nice colour plots of fixation times/probabilities at the different absorbing states and for different initial conditions in the asymmetric game case. Should we not put these in and make this the main focus?

Table 4: What does `ts’ mean?

Many of the figures are too small to read, in particular also Fig 6 and 7. I’d recommend that you print the PDF and look at the printout to make sure all axes labels, tick labels, flow lines and other features are clearly visible.

These comments may sound overly critical — sorry this is not my intention. I think we have a very nice story here, it is just a matter of finding the best way to actually tell it, and to make sure that the key audience (people working in theoretical biology, evolutionary dynamics in finite populations) understand it.

Hope this helps. At some point later I’ll obviously also be happy to make edits, but maybe it is best to finalise the figures first.

Tobias


Tobias Galla
School of Physics and Astronomy
The University of Manchester
+44-(0)161-275-4264
http://www.theory.physics.manchester.ac.uk/~galla
@tobiasgalla



> On 12 Dec 2015, at 19:21, Robin NICOLE <robin.nicole.m@gmail.com> wrote:
>
> Hi Tobias, 
> I received some comments from peter and hence I am updating the article, I do it via overleaf which is more stable than share latex, you can have a look at it on https://www.overleaf.com/3906480xmqrnm 
> Wishes 
> Robin


